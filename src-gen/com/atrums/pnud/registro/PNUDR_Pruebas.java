/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Location;
/**
 * Entity class for entity PNUDR_pruebas (stored in table PNUDR_Pruebas).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Pruebas extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Pruebas";
    public static final String ENTITY_NAME = "PNUDR_pruebas";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_CODIGO = "codigo";
    public static final String PROPERTY_FECHAMUESTRA = "fechaMuestra";
    public static final String PROPERTY_TIPOPRUEBA = "tipoPrueba";
    public static final String PROPERTY_RESULTADO = "resultado";
    public static final String PROPERTY_RESPONSABLE = "responsable";
    public static final String PROPERTY_LOCATIONADDRESS = "locationAddress";
    public static final String PROPERTY_OBSERVACION = "observacion";
    public static final String PROPERTY_TRATAMIENTO = "tratamiento";
    public static final String PROPERTY_PNUDRREGEQUIPO = "pnudrRegEquipo";
    public static final String PROPERTY_CONCENTRACION = "concentracion";
    public static final String PROPERTY_PNUDRREGACEITE = "pnudrRegAceite";
    public static final String PROPERTY_BPARTNER = "bpartner";
    public static final String PROPERTY_BAJO = "bajo";

    public PNUDR_Pruebas() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_BAJO, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getCodigo() {
        return (String) get(PROPERTY_CODIGO);
    }

    public void setCodigo(String codigo) {
        set(PROPERTY_CODIGO, codigo);
    }

    public Date getFechaMuestra() {
        return (Date) get(PROPERTY_FECHAMUESTRA);
    }

    public void setFechaMuestra(Date fechaMuestra) {
        set(PROPERTY_FECHAMUESTRA, fechaMuestra);
    }

    public String getTipoPrueba() {
        return (String) get(PROPERTY_TIPOPRUEBA);
    }

    public void setTipoPrueba(String tipoPrueba) {
        set(PROPERTY_TIPOPRUEBA, tipoPrueba);
    }

    public String getResultado() {
        return (String) get(PROPERTY_RESULTADO);
    }

    public void setResultado(String resultado) {
        set(PROPERTY_RESULTADO, resultado);
    }

    public String getResponsable() {
        return (String) get(PROPERTY_RESPONSABLE);
    }

    public void setResponsable(String responsable) {
        set(PROPERTY_RESPONSABLE, responsable);
    }

    public Location getLocationAddress() {
        return (Location) get(PROPERTY_LOCATIONADDRESS);
    }

    public void setLocationAddress(Location locationAddress) {
        set(PROPERTY_LOCATIONADDRESS, locationAddress);
    }

    public String getObservacion() {
        return (String) get(PROPERTY_OBSERVACION);
    }

    public void setObservacion(String observacion) {
        set(PROPERTY_OBSERVACION, observacion);
    }

    public String getTratamiento() {
        return (String) get(PROPERTY_TRATAMIENTO);
    }

    public void setTratamiento(String tratamiento) {
        set(PROPERTY_TRATAMIENTO, tratamiento);
    }

    public PNUDR_Reg_Equipo getPnudrRegEquipo() {
        return (PNUDR_Reg_Equipo) get(PROPERTY_PNUDRREGEQUIPO);
    }

    public void setPnudrRegEquipo(PNUDR_Reg_Equipo pnudrRegEquipo) {
        set(PROPERTY_PNUDRREGEQUIPO, pnudrRegEquipo);
    }

    public BigDecimal getConcentracion() {
        return (BigDecimal) get(PROPERTY_CONCENTRACION);
    }

    public void setConcentracion(BigDecimal concentracion) {
        set(PROPERTY_CONCENTRACION, concentracion);
    }

    public PNUDR_Reg_Aceite getPnudrRegAceite() {
        return (PNUDR_Reg_Aceite) get(PROPERTY_PNUDRREGACEITE);
    }

    public void setPnudrRegAceite(PNUDR_Reg_Aceite pnudrRegAceite) {
        set(PROPERTY_PNUDRREGACEITE, pnudrRegAceite);
    }

    public BusinessPartner getBpartner() {
        return (BusinessPartner) get(PROPERTY_BPARTNER);
    }

    public void setBpartner(BusinessPartner bpartner) {
        set(PROPERTY_BPARTNER, bpartner);
    }

    public Boolean isBajo() {
        return (Boolean) get(PROPERTY_BAJO);
    }

    public void setBajo(Boolean bajo) {
        set(PROPERTY_BAJO, bajo);
    }

}
