/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.geography.Location;
/**
 * Entity class for entity PNUDR_ubic (stored in table PNUDR_Ubic).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Ubic extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Ubic";
    public static final String ENTITY_NAME = "PNUDR_ubic";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PNUDRREGACEITE = "pnudrRegAceite";
    public static final String PROPERTY_TIPOUBICACION = "tipoUbicacion";
    public static final String PROPERTY_LOCATIONADDRESS = "locationAddress";
    public static final String PROPERTY_LATITUD = "latitud";
    public static final String PROPERTY_LONGITUD = "longitud";
    public static final String PROPERTY_NROPORTE = "nROPorte";
    public static final String PROPERTY_NROALIMENTADOR = "nROAlimentador";
    public static final String PROPERTY_FECHAUBICACION = "fechaUbicacion";
    public static final String PROPERTY_USERCONTACT = "userContact";
    public static final String PROPERTY_OBSERVACION = "observacion";
    public static final String PROPERTY_PNUDRREGSOLIDO = "pnudrRegSolido";
    public static final String PROPERTY_RESPONSABLE = "responsable";
    public static final String PROPERTY_WAREHOUSE = "warehouse";
    public static final String PROPERTY_PNUDRZONA = "pnudrZona";

    public PNUDR_Ubic() {
        setDefaultValue(PROPERTY_ACTIVE, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public PNUDR_Reg_Aceite getPnudrRegAceite() {
        return (PNUDR_Reg_Aceite) get(PROPERTY_PNUDRREGACEITE);
    }

    public void setPnudrRegAceite(PNUDR_Reg_Aceite pnudrRegAceite) {
        set(PROPERTY_PNUDRREGACEITE, pnudrRegAceite);
    }

    public String getTipoUbicacion() {
        return (String) get(PROPERTY_TIPOUBICACION);
    }

    public void setTipoUbicacion(String tipoUbicacion) {
        set(PROPERTY_TIPOUBICACION, tipoUbicacion);
    }

    public Location getLocationAddress() {
        return (Location) get(PROPERTY_LOCATIONADDRESS);
    }

    public void setLocationAddress(Location locationAddress) {
        set(PROPERTY_LOCATIONADDRESS, locationAddress);
    }

    public Long getLatitud() {
        return (Long) get(PROPERTY_LATITUD);
    }

    public void setLatitud(Long latitud) {
        set(PROPERTY_LATITUD, latitud);
    }

    public Long getLongitud() {
        return (Long) get(PROPERTY_LONGITUD);
    }

    public void setLongitud(Long longitud) {
        set(PROPERTY_LONGITUD, longitud);
    }

    public String getNROPorte() {
        return (String) get(PROPERTY_NROPORTE);
    }

    public void setNROPorte(String nROPorte) {
        set(PROPERTY_NROPORTE, nROPorte);
    }

    public String getNROAlimentador() {
        return (String) get(PROPERTY_NROALIMENTADOR);
    }

    public void setNROAlimentador(String nROAlimentador) {
        set(PROPERTY_NROALIMENTADOR, nROAlimentador);
    }

    public Date getFechaUbicacion() {
        return (Date) get(PROPERTY_FECHAUBICACION);
    }

    public void setFechaUbicacion(Date fechaUbicacion) {
        set(PROPERTY_FECHAUBICACION, fechaUbicacion);
    }

    public User getUserContact() {
        return (User) get(PROPERTY_USERCONTACT);
    }

    public void setUserContact(User userContact) {
        set(PROPERTY_USERCONTACT, userContact);
    }

    public String getObservacion() {
        return (String) get(PROPERTY_OBSERVACION);
    }

    public void setObservacion(String observacion) {
        set(PROPERTY_OBSERVACION, observacion);
    }

    public PNUDR_Reg_Solido getPnudrRegSolido() {
        return (PNUDR_Reg_Solido) get(PROPERTY_PNUDRREGSOLIDO);
    }

    public void setPnudrRegSolido(PNUDR_Reg_Solido pnudrRegSolido) {
        set(PROPERTY_PNUDRREGSOLIDO, pnudrRegSolido);
    }

    public String getResponsable() {
        return (String) get(PROPERTY_RESPONSABLE);
    }

    public void setResponsable(String responsable) {
        set(PROPERTY_RESPONSABLE, responsable);
    }

    public Warehouse getWarehouse() {
        return (Warehouse) get(PROPERTY_WAREHOUSE);
    }

    public void setWarehouse(Warehouse warehouse) {
        set(PROPERTY_WAREHOUSE, warehouse);
    }

    public PNUDR_Zona getPnudrZona() {
        return (PNUDR_Zona) get(PROPERTY_PNUDRZONA);
    }

    public void setPnudrZona(PNUDR_Zona pnudrZona) {
        set(PROPERTY_PNUDRZONA, pnudrZona);
    }

}
