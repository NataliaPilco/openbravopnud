/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity PNUDR_reg_solido (stored in table PNUDR_Reg_Solido).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Reg_Solido extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Reg_Solido";
    public static final String ENTITY_NAME = "PNUDR_reg_solido";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PNUDRCONFGEQUIPO = "pnudrConfgEquipo";
    public static final String PROPERTY_CAPACIDAD = "capacidad";
    public static final String PROPERTY_VOLUMENDISPONIBLE = "volumenDisponible";
    public static final String PROPERTY_VOLUMENUTILIZADO = "volumenUtilizado";
    public static final String PROPERTY_NROSERIE = "nROSerie";
    public static final String PROPERTY_NROSERIEELECTRICA = "nROSerieElectrica";
    public static final String PROPERTY_PNUDRNROSERIESOLLIST = "pNUDRNroSerieSolList";
    public static final String PROPERTY_PNUDRTRAINVLIST = "pNUDRTraInvList";
    public static final String PROPERTY_PNUDRUBICLIST = "pNUDRUbicList";

    public PNUDR_Reg_Solido() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_CAPACIDAD, new BigDecimal(0));
        setDefaultValue(PROPERTY_VOLUMENDISPONIBLE, (long) 0);
        setDefaultValue(PROPERTY_VOLUMENUTILIZADO, new BigDecimal(0));
        setDefaultValue(PROPERTY_PNUDRNROSERIESOLLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRTRAINVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRUBICLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public PNUDR_Confg_Equipo getPnudrConfgEquipo() {
        return (PNUDR_Confg_Equipo) get(PROPERTY_PNUDRCONFGEQUIPO);
    }

    public void setPnudrConfgEquipo(PNUDR_Confg_Equipo pnudrConfgEquipo) {
        set(PROPERTY_PNUDRCONFGEQUIPO, pnudrConfgEquipo);
    }

    public BigDecimal getCapacidad() {
        return (BigDecimal) get(PROPERTY_CAPACIDAD);
    }

    public void setCapacidad(BigDecimal capacidad) {
        set(PROPERTY_CAPACIDAD, capacidad);
    }

    public Long getVolumenDisponible() {
        return (Long) get(PROPERTY_VOLUMENDISPONIBLE);
    }

    public void setVolumenDisponible(Long volumenDisponible) {
        set(PROPERTY_VOLUMENDISPONIBLE, volumenDisponible);
    }

    public BigDecimal getVolumenUtilizado() {
        return (BigDecimal) get(PROPERTY_VOLUMENUTILIZADO);
    }

    public void setVolumenUtilizado(BigDecimal volumenUtilizado) {
        set(PROPERTY_VOLUMENUTILIZADO, volumenUtilizado);
    }

    public String getNROSerie() {
        return (String) get(PROPERTY_NROSERIE);
    }

    public void setNROSerie(String nROSerie) {
        set(PROPERTY_NROSERIE, nROSerie);
    }

    public String getNROSerieElectrica() {
        return (String) get(PROPERTY_NROSERIEELECTRICA);
    }

    public void setNROSerieElectrica(String nROSerieElectrica) {
        set(PROPERTY_NROSERIEELECTRICA, nROSerieElectrica);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Nro_Serie_Sol> getPNUDRNroSerieSolList() {
      return (List<PNUDR_Nro_Serie_Sol>) get(PROPERTY_PNUDRNROSERIESOLLIST);
    }

    public void setPNUDRNroSerieSolList(List<PNUDR_Nro_Serie_Sol> pNUDRNroSerieSolList) {
        set(PROPERTY_PNUDRNROSERIESOLLIST, pNUDRNroSerieSolList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Tra_Inv> getPNUDRTraInvList() {
      return (List<PNUDR_Tra_Inv>) get(PROPERTY_PNUDRTRAINVLIST);
    }

    public void setPNUDRTraInvList(List<PNUDR_Tra_Inv> pNUDRTraInvList) {
        set(PROPERTY_PNUDRTRAINVLIST, pNUDRTraInvList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Ubic> getPNUDRUbicList() {
      return (List<PNUDR_Ubic>) get(PROPERTY_PNUDRUBICLIST);
    }

    public void setPNUDRUbicList(List<PNUDR_Ubic> pNUDRUbicList) {
        set(PROPERTY_PNUDRUBICLIST, pNUDRUbicList);
    }

}
