/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.financialmgmt.calendar.Year;
/**
 * Entity class for entity PNUDR_reg_equipo (stored in table PNUDR_Reg_Equipo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Reg_Equipo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Reg_Equipo";
    public static final String ENTITY_NAME = "PNUDR_reg_equipo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_NROPLACA = "nROPlaca";
    public static final String PROPERTY_NROSERIE = "nROSerie";
    public static final String PROPERTY_NROEMPRESA = "nROEmpresa";
    public static final String PROPERTY_PNUDRMARCA = "pnudrMarca";
    public static final String PROPERTY_MODELO = "modelo";
    public static final String PROPERTY_KVA = "kva";
    public static final String PROPERTY_PNUDRCONFGEQUIPO = "pnudrConfgEquipo";
    public static final String PROPERTY_TIPOTRANSFORMADOR = "tipoTransformador";
    public static final String PROPERTY_NROFASES = "nROFases";
    public static final String PROPERTY_YEAR = "year";
    public static final String PROPERTY_COUNTRY = "country";
    public static final String PROPERTY_RECONTRUIDO = "recontruido";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_ESTADOOPERATIVO = "estadoOperativo";
    public static final String PROPERTY_ESTADONOOPERATIVO = "estadoNoOperativo";
    public static final String PROPERTY_BTNIMP = "bTNImp";
    public static final String PROPERTY_EQUIPOACEITE = "equipoAceite";
    public static final String PROPERTY_KVAKG = "kvakg";
    public static final String PROPERTY_ESTATUS = "estatus";
    public static final String PROPERTY_ACEITE = "aceite";
    public static final String PROPERTY_EQUIPOSINACEITE = "equipoSinAceite";
    public static final String PROPERTY_LITROS = "litros";
    public static final String PROPERTY_PNUDRNROEMPRESALIST = "pNUDRNroEmpresaList";
    public static final String PROPERTY_PNUDRPRUEBASLIST = "pNUDRPruebasList";
    public static final String PROPERTY_PNUDRTRAINVLIST = "pNUDRTraInvList";
    public static final String PROPERTY_PNUDRUBICPROPPESOLIST = "pNUDRUbicproppesoList";

    public PNUDR_Reg_Equipo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_KVA, new BigDecimal(0));
        setDefaultValue(PROPERTY_RECONTRUIDO, false);
        setDefaultValue(PROPERTY_BTNIMP, false);
        setDefaultValue(PROPERTY_EQUIPOACEITE, new BigDecimal(0));
        setDefaultValue(PROPERTY_KVAKG, new BigDecimal(0));
        setDefaultValue(PROPERTY_ACEITE, new BigDecimal(0));
        setDefaultValue(PROPERTY_EQUIPOSINACEITE, new BigDecimal(0));
        setDefaultValue(PROPERTY_LITROS, new BigDecimal(0));
        setDefaultValue(PROPERTY_PNUDRNROEMPRESALIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRPRUEBASLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRTRAINVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRUBICPROPPESOLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public String getNROPlaca() {
        return (String) get(PROPERTY_NROPLACA);
    }

    public void setNROPlaca(String nROPlaca) {
        set(PROPERTY_NROPLACA, nROPlaca);
    }

    public String getNROSerie() {
        return (String) get(PROPERTY_NROSERIE);
    }

    public void setNROSerie(String nROSerie) {
        set(PROPERTY_NROSERIE, nROSerie);
    }

    public String getNROEmpresa() {
        return (String) get(PROPERTY_NROEMPRESA);
    }

    public void setNROEmpresa(String nROEmpresa) {
        set(PROPERTY_NROEMPRESA, nROEmpresa);
    }

    public PNUDR_Marca getPnudrMarca() {
        return (PNUDR_Marca) get(PROPERTY_PNUDRMARCA);
    }

    public void setPnudrMarca(PNUDR_Marca pnudrMarca) {
        set(PROPERTY_PNUDRMARCA, pnudrMarca);
    }

    public String getModelo() {
        return (String) get(PROPERTY_MODELO);
    }

    public void setModelo(String modelo) {
        set(PROPERTY_MODELO, modelo);
    }

    public BigDecimal getKva() {
        return (BigDecimal) get(PROPERTY_KVA);
    }

    public void setKva(BigDecimal kva) {
        set(PROPERTY_KVA, kva);
    }

    public PNUDR_Confg_Equipo getPnudrConfgEquipo() {
        return (PNUDR_Confg_Equipo) get(PROPERTY_PNUDRCONFGEQUIPO);
    }

    public void setPnudrConfgEquipo(PNUDR_Confg_Equipo pnudrConfgEquipo) {
        set(PROPERTY_PNUDRCONFGEQUIPO, pnudrConfgEquipo);
    }

    public String getTipoTransformador() {
        return (String) get(PROPERTY_TIPOTRANSFORMADOR);
    }

    public void setTipoTransformador(String tipoTransformador) {
        set(PROPERTY_TIPOTRANSFORMADOR, tipoTransformador);
    }

    public String getNROFases() {
        return (String) get(PROPERTY_NROFASES);
    }

    public void setNROFases(String nROFases) {
        set(PROPERTY_NROFASES, nROFases);
    }

    public Year getYear() {
        return (Year) get(PROPERTY_YEAR);
    }

    public void setYear(Year year) {
        set(PROPERTY_YEAR, year);
    }

    public Country getCountry() {
        return (Country) get(PROPERTY_COUNTRY);
    }

    public void setCountry(Country country) {
        set(PROPERTY_COUNTRY, country);
    }

    public Boolean isRecontruido() {
        return (Boolean) get(PROPERTY_RECONTRUIDO);
    }

    public void setRecontruido(Boolean recontruido) {
        set(PROPERTY_RECONTRUIDO, recontruido);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public String getEstadoOperativo() {
        return (String) get(PROPERTY_ESTADOOPERATIVO);
    }

    public void setEstadoOperativo(String estadoOperativo) {
        set(PROPERTY_ESTADOOPERATIVO, estadoOperativo);
    }

    public String getEstadoNoOperativo() {
        return (String) get(PROPERTY_ESTADONOOPERATIVO);
    }

    public void setEstadoNoOperativo(String estadoNoOperativo) {
        set(PROPERTY_ESTADONOOPERATIVO, estadoNoOperativo);
    }

    public Boolean isBTNImp() {
        return (Boolean) get(PROPERTY_BTNIMP);
    }

    public void setBTNImp(Boolean bTNImp) {
        set(PROPERTY_BTNIMP, bTNImp);
    }

    public BigDecimal getEquipoAceite() {
        return (BigDecimal) get(PROPERTY_EQUIPOACEITE);
    }

    public void setEquipoAceite(BigDecimal equipoAceite) {
        set(PROPERTY_EQUIPOACEITE, equipoAceite);
    }

    public BigDecimal getKvakg() {
        return (BigDecimal) get(PROPERTY_KVAKG);
    }

    public void setKvakg(BigDecimal kvakg) {
        set(PROPERTY_KVAKG, kvakg);
    }

    public String getEstatus() {
        return (String) get(PROPERTY_ESTATUS);
    }

    public void setEstatus(String estatus) {
        set(PROPERTY_ESTATUS, estatus);
    }

    public BigDecimal getAceite() {
        return (BigDecimal) get(PROPERTY_ACEITE);
    }

    public void setAceite(BigDecimal aceite) {
        set(PROPERTY_ACEITE, aceite);
    }

    public BigDecimal getEquipoSinAceite() {
        return (BigDecimal) get(PROPERTY_EQUIPOSINACEITE);
    }

    public void setEquipoSinAceite(BigDecimal equipoSinAceite) {
        set(PROPERTY_EQUIPOSINACEITE, equipoSinAceite);
    }

    public BigDecimal getLitros() {
        return (BigDecimal) get(PROPERTY_LITROS);
    }

    public void setLitros(BigDecimal litros) {
        set(PROPERTY_LITROS, litros);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Nro_Empresa> getPNUDRNroEmpresaList() {
      return (List<PNUDR_Nro_Empresa>) get(PROPERTY_PNUDRNROEMPRESALIST);
    }

    public void setPNUDRNroEmpresaList(List<PNUDR_Nro_Empresa> pNUDRNroEmpresaList) {
        set(PROPERTY_PNUDRNROEMPRESALIST, pNUDRNroEmpresaList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Pruebas> getPNUDRPruebasList() {
      return (List<PNUDR_Pruebas>) get(PROPERTY_PNUDRPRUEBASLIST);
    }

    public void setPNUDRPruebasList(List<PNUDR_Pruebas> pNUDRPruebasList) {
        set(PROPERTY_PNUDRPRUEBASLIST, pNUDRPruebasList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Tra_Inv> getPNUDRTraInvList() {
      return (List<PNUDR_Tra_Inv>) get(PROPERTY_PNUDRTRAINVLIST);
    }

    public void setPNUDRTraInvList(List<PNUDR_Tra_Inv> pNUDRTraInvList) {
        set(PROPERTY_PNUDRTRAINVLIST, pNUDRTraInvList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_UbicPropPeso> getPNUDRUbicproppesoList() {
      return (List<PNUDR_UbicPropPeso>) get(PROPERTY_PNUDRUBICPROPPESOLIST);
    }

    public void setPNUDRUbicproppesoList(List<PNUDR_UbicPropPeso> pNUDRUbicproppesoList) {
        set(PROPERTY_PNUDRUBICPROPPESOLIST, pNUDRUbicproppesoList);
    }

}
