/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Image;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity PNUDR_confg_equipo (stored in table PNUDR_Confg_Equipo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Confg_Equipo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Confg_Equipo";
    public static final String ENTITY_NAME = "PNUDR_confg_equipo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LINEA = "linea";
    public static final String PROPERTY_TIPOEQUIPO = "tipoEquipo";
    public static final String PROPERTY_GROUPCONF = "groupConf";
    public static final String PROPERTY_TIPOTRANSF = "tipoTransf";
    public static final String PROPERTY_NROFASES = "nROFases";
    public static final String PROPERTY_CAPACIDADLT = "capacidadLt";
    public static final String PROPERTY_CAPACIDADGL = "capacidadGl";
    public static final String PROPERTY_PESOMAXIMO = "pesoMaximo";
    public static final String PROPERTY_GROUPCONFEQU = "groupConfEqu";
    public static final String PROPERTY_GROUPCONFACE = "groupConfAce";
    public static final String PROPERTY_GROUPCONFSOL = "groupConfSol";
    public static final String PROPERTY_IMAGE = "image";
    public static final String PROPERTY_PNUDREQUIPOSVLIST = "pNUDREquiposVList";
    public static final String PROPERTY_PNUDRREGACEITELIST = "pNUDRRegAceiteList";
    public static final String PROPERTY_PNUDRREGEQUIPOLIST = "pNUDRRegEquipoList";
    public static final String PROPERTY_PNUDRREGSOLIDOLIST = "pNUDRRegSolidoList";
    public static final String PROPERTY_PNUDRTRAINVLIST = "pNUDRTraInvList";

    public PNUDR_Confg_Equipo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_CAPACIDADLT, new BigDecimal(0));
        setDefaultValue(PROPERTY_CAPACIDADGL, new BigDecimal(0));
        setDefaultValue(PROPERTY_PESOMAXIMO, (long) 0);
        setDefaultValue(PROPERTY_PNUDREQUIPOSVLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRREGACEITELIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRREGEQUIPOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRREGSOLIDOLIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRTRAINVLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Long getLinea() {
        return (Long) get(PROPERTY_LINEA);
    }

    public void setLinea(Long linea) {
        set(PROPERTY_LINEA, linea);
    }

    public String getTipoEquipo() {
        return (String) get(PROPERTY_TIPOEQUIPO);
    }

    public void setTipoEquipo(String tipoEquipo) {
        set(PROPERTY_TIPOEQUIPO, tipoEquipo);
    }

    public String getGroupConf() {
        return (String) get(PROPERTY_GROUPCONF);
    }

    public void setGroupConf(String groupConf) {
        set(PROPERTY_GROUPCONF, groupConf);
    }

    public String getTipoTransf() {
        return (String) get(PROPERTY_TIPOTRANSF);
    }

    public void setTipoTransf(String tipoTransf) {
        set(PROPERTY_TIPOTRANSF, tipoTransf);
    }

    public String getNROFases() {
        return (String) get(PROPERTY_NROFASES);
    }

    public void setNROFases(String nROFases) {
        set(PROPERTY_NROFASES, nROFases);
    }

    public BigDecimal getCapacidadLt() {
        return (BigDecimal) get(PROPERTY_CAPACIDADLT);
    }

    public void setCapacidadLt(BigDecimal capacidadLt) {
        set(PROPERTY_CAPACIDADLT, capacidadLt);
    }

    public BigDecimal getCapacidadGl() {
        return (BigDecimal) get(PROPERTY_CAPACIDADGL);
    }

    public void setCapacidadGl(BigDecimal capacidadGl) {
        set(PROPERTY_CAPACIDADGL, capacidadGl);
    }

    public Long getPesoMaximo() {
        return (Long) get(PROPERTY_PESOMAXIMO);
    }

    public void setPesoMaximo(Long pesoMaximo) {
        set(PROPERTY_PESOMAXIMO, pesoMaximo);
    }

    public PNUDR_Group_Equipo getGroupConfEqu() {
        return (PNUDR_Group_Equipo) get(PROPERTY_GROUPCONFEQU);
    }

    public void setGroupConfEqu(PNUDR_Group_Equipo groupConfEqu) {
        set(PROPERTY_GROUPCONFEQU, groupConfEqu);
    }

    public PNUDR_Group_Equipo getGroupConfAce() {
        return (PNUDR_Group_Equipo) get(PROPERTY_GROUPCONFACE);
    }

    public void setGroupConfAce(PNUDR_Group_Equipo groupConfAce) {
        set(PROPERTY_GROUPCONFACE, groupConfAce);
    }

    public PNUDR_Group_Equipo getGroupConfSol() {
        return (PNUDR_Group_Equipo) get(PROPERTY_GROUPCONFSOL);
    }

    public void setGroupConfSol(PNUDR_Group_Equipo groupConfSol) {
        set(PROPERTY_GROUPCONFSOL, groupConfSol);
    }

    public Image getImage() {
        return (Image) get(PROPERTY_IMAGE);
    }

    public void setImage(Image image) {
        set(PROPERTY_IMAGE, image);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Equipos_V> getPNUDREquiposVList() {
      return (List<PNUDR_Equipos_V>) get(PROPERTY_PNUDREQUIPOSVLIST);
    }

    public void setPNUDREquiposVList(List<PNUDR_Equipos_V> pNUDREquiposVList) {
        set(PROPERTY_PNUDREQUIPOSVLIST, pNUDREquiposVList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Reg_Aceite> getPNUDRRegAceiteList() {
      return (List<PNUDR_Reg_Aceite>) get(PROPERTY_PNUDRREGACEITELIST);
    }

    public void setPNUDRRegAceiteList(List<PNUDR_Reg_Aceite> pNUDRRegAceiteList) {
        set(PROPERTY_PNUDRREGACEITELIST, pNUDRRegAceiteList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Reg_Equipo> getPNUDRRegEquipoList() {
      return (List<PNUDR_Reg_Equipo>) get(PROPERTY_PNUDRREGEQUIPOLIST);
    }

    public void setPNUDRRegEquipoList(List<PNUDR_Reg_Equipo> pNUDRRegEquipoList) {
        set(PROPERTY_PNUDRREGEQUIPOLIST, pNUDRRegEquipoList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Reg_Solido> getPNUDRRegSolidoList() {
      return (List<PNUDR_Reg_Solido>) get(PROPERTY_PNUDRREGSOLIDOLIST);
    }

    public void setPNUDRRegSolidoList(List<PNUDR_Reg_Solido> pNUDRRegSolidoList) {
        set(PROPERTY_PNUDRREGSOLIDOLIST, pNUDRRegSolidoList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Tra_Inv> getPNUDRTraInvList() {
      return (List<PNUDR_Tra_Inv>) get(PROPERTY_PNUDRTRAINVLIST);
    }

    public void setPNUDRTraInvList(List<PNUDR_Tra_Inv> pNUDRTraInvList) {
        set(PROPERTY_PNUDRTRAINVLIST, pNUDRTraInvList);
    }

}
