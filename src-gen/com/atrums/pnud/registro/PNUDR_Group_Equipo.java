/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity PNUDR_group_equipo (stored in table PNUDR_Group_Equipo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_Group_Equipo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_Group_Equipo";
    public static final String ENTITY_NAME = "PNUDR_group_equipo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_LINEA = "linea";
    public static final String PROPERTY_GRUPO = "grupo";
    public static final String PROPERTY_GRUPOCONF = "grupoConf";
    public static final String PROPERTY_PNUDRCONFGEQUIPOGROUPCONFEQULIST = "pNUDRConfgEquipoGroupConfEquList";
    public static final String PROPERTY_PNUDRCONFGEQUIPOGROUPCONFACELIST = "pNUDRConfgEquipoGroupConfAceList";
    public static final String PROPERTY_PNUDRCONFGEQUIPOGROUPCONFSOLLIST = "pNUDRConfgEquipoGroupConfSolList";

    public PNUDR_Group_Equipo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFEQULIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFACELIST, new ArrayList<Object>());
        setDefaultValue(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFSOLLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Long getLinea() {
        return (Long) get(PROPERTY_LINEA);
    }

    public void setLinea(Long linea) {
        set(PROPERTY_LINEA, linea);
    }

    public String getGrupo() {
        return (String) get(PROPERTY_GRUPO);
    }

    public void setGrupo(String grupo) {
        set(PROPERTY_GRUPO, grupo);
    }

    public String getGrupoConf() {
        return (String) get(PROPERTY_GRUPOCONF);
    }

    public void setGrupoConf(String grupoConf) {
        set(PROPERTY_GRUPOCONF, grupoConf);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Confg_Equipo> getPNUDRConfgEquipoGroupConfEquList() {
      return (List<PNUDR_Confg_Equipo>) get(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFEQULIST);
    }

    public void setPNUDRConfgEquipoGroupConfEquList(List<PNUDR_Confg_Equipo> pNUDRConfgEquipoGroupConfEquList) {
        set(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFEQULIST, pNUDRConfgEquipoGroupConfEquList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Confg_Equipo> getPNUDRConfgEquipoGroupConfAceList() {
      return (List<PNUDR_Confg_Equipo>) get(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFACELIST);
    }

    public void setPNUDRConfgEquipoGroupConfAceList(List<PNUDR_Confg_Equipo> pNUDRConfgEquipoGroupConfAceList) {
        set(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFACELIST, pNUDRConfgEquipoGroupConfAceList);
    }

    @SuppressWarnings("unchecked")
    public List<PNUDR_Confg_Equipo> getPNUDRConfgEquipoGroupConfSolList() {
      return (List<PNUDR_Confg_Equipo>) get(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFSOLLIST);
    }

    public void setPNUDRConfgEquipoGroupConfSolList(List<PNUDR_Confg_Equipo> pNUDRConfgEquipoGroupConfSolList) {
        set(PROPERTY_PNUDRCONFGEQUIPOGROUPCONFSOLLIST, pNUDRConfgEquipoGroupConfSolList);
    }

}
