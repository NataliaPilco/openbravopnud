/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.pnud.registro;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.geography.Location;
/**
 * Entity class for entity PNUDR_ubicproppeso (stored in table PNUDR_UbicPropPeso).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class PNUDR_UbicPropPeso extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "PNUDR_UbicPropPeso";
    public static final String ENTITY_NAME = "PNUDR_ubicproppeso";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PNUDRREGEQUIPO = "pnudrRegEquipo";
    public static final String PROPERTY_TIPOUBICACION = "tipoUbicacion";
    public static final String PROPERTY_LOCATIONADDRESS = "locationAddress";
    public static final String PROPERTY_LATITUD = "latitud";
    public static final String PROPERTY_LONGITUD = "longitud";
    public static final String PROPERTY_NROPORTE = "nROPorte";
    public static final String PROPERTY_NROALIMENTADOR = "nROAlimentador";
    public static final String PROPERTY_FECHAUBICACION = "fechaUbicacion";
    public static final String PROPERTY_USERCONTACT = "userContact";
    public static final String PROPERTY_OBSERVACION = "observacion";
    public static final String PROPERTY_BPARTNER = "bpartner";
    public static final String PROPERTY_COMPLETO = "completo";
    public static final String PROPERTY_FECHAINICIO = "fechaInicio";
    public static final String PROPERTY_FECHAFIN = "fechaFin";
    public static final String PROPERTY_FECHAPESAJE = "fechaPesaje";
    public static final String PROPERTY_USERACEITE = "userAceite";
    public static final String PROPERTY_OBSERVACIONACEITE = "observacionAceite";
    public static final String PROPERTY_PNUDRREGACEITE = "pnudrRegAceite";
    public static final String PROPERTY_WAREHOUSE = "warehouse";
    public static final String PROPERTY_LITROS = "litros";
    public static final String PROPERTY_CONTENEDOR = "contenedor";
    public static final String PROPERTY_PNUDRZONA = "pnudrZona";

    public PNUDR_UbicPropPeso() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_COMPLETO, true);
        setDefaultValue(PROPERTY_LITROS, new BigDecimal(0));
        setDefaultValue(PROPERTY_CONTENEDOR, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public PNUDR_Reg_Equipo getPnudrRegEquipo() {
        return (PNUDR_Reg_Equipo) get(PROPERTY_PNUDRREGEQUIPO);
    }

    public void setPnudrRegEquipo(PNUDR_Reg_Equipo pnudrRegEquipo) {
        set(PROPERTY_PNUDRREGEQUIPO, pnudrRegEquipo);
    }

    public String getTipoUbicacion() {
        return (String) get(PROPERTY_TIPOUBICACION);
    }

    public void setTipoUbicacion(String tipoUbicacion) {
        set(PROPERTY_TIPOUBICACION, tipoUbicacion);
    }

    public Location getLocationAddress() {
        return (Location) get(PROPERTY_LOCATIONADDRESS);
    }

    public void setLocationAddress(Location locationAddress) {
        set(PROPERTY_LOCATIONADDRESS, locationAddress);
    }

    public Long getLatitud() {
        return (Long) get(PROPERTY_LATITUD);
    }

    public void setLatitud(Long latitud) {
        set(PROPERTY_LATITUD, latitud);
    }

    public Long getLongitud() {
        return (Long) get(PROPERTY_LONGITUD);
    }

    public void setLongitud(Long longitud) {
        set(PROPERTY_LONGITUD, longitud);
    }

    public String getNROPorte() {
        return (String) get(PROPERTY_NROPORTE);
    }

    public void setNROPorte(String nROPorte) {
        set(PROPERTY_NROPORTE, nROPorte);
    }

    public String getNROAlimentador() {
        return (String) get(PROPERTY_NROALIMENTADOR);
    }

    public void setNROAlimentador(String nROAlimentador) {
        set(PROPERTY_NROALIMENTADOR, nROAlimentador);
    }

    public Date getFechaUbicacion() {
        return (Date) get(PROPERTY_FECHAUBICACION);
    }

    public void setFechaUbicacion(Date fechaUbicacion) {
        set(PROPERTY_FECHAUBICACION, fechaUbicacion);
    }

    public User getUserContact() {
        return (User) get(PROPERTY_USERCONTACT);
    }

    public void setUserContact(User userContact) {
        set(PROPERTY_USERCONTACT, userContact);
    }

    public String getObservacion() {
        return (String) get(PROPERTY_OBSERVACION);
    }

    public void setObservacion(String observacion) {
        set(PROPERTY_OBSERVACION, observacion);
    }

    public BusinessPartner getBpartner() {
        return (BusinessPartner) get(PROPERTY_BPARTNER);
    }

    public void setBpartner(BusinessPartner bpartner) {
        set(PROPERTY_BPARTNER, bpartner);
    }

    public Boolean isCompleto() {
        return (Boolean) get(PROPERTY_COMPLETO);
    }

    public void setCompleto(Boolean completo) {
        set(PROPERTY_COMPLETO, completo);
    }

    public Date getFechaInicio() {
        return (Date) get(PROPERTY_FECHAINICIO);
    }

    public void setFechaInicio(Date fechaInicio) {
        set(PROPERTY_FECHAINICIO, fechaInicio);
    }

    public Date getFechaFin() {
        return (Date) get(PROPERTY_FECHAFIN);
    }

    public void setFechaFin(Date fechaFin) {
        set(PROPERTY_FECHAFIN, fechaFin);
    }

    public Date getFechaPesaje() {
        return (Date) get(PROPERTY_FECHAPESAJE);
    }

    public void setFechaPesaje(Date fechaPesaje) {
        set(PROPERTY_FECHAPESAJE, fechaPesaje);
    }

    public User getUserAceite() {
        return (User) get(PROPERTY_USERACEITE);
    }

    public void setUserAceite(User userAceite) {
        set(PROPERTY_USERACEITE, userAceite);
    }

    public String getObservacionAceite() {
        return (String) get(PROPERTY_OBSERVACIONACEITE);
    }

    public void setObservacionAceite(String observacionAceite) {
        set(PROPERTY_OBSERVACIONACEITE, observacionAceite);
    }

    public PNUDR_Reg_Aceite getPnudrRegAceite() {
        return (PNUDR_Reg_Aceite) get(PROPERTY_PNUDRREGACEITE);
    }

    public void setPnudrRegAceite(PNUDR_Reg_Aceite pnudrRegAceite) {
        set(PROPERTY_PNUDRREGACEITE, pnudrRegAceite);
    }

    public Warehouse getWarehouse() {
        return (Warehouse) get(PROPERTY_WAREHOUSE);
    }

    public void setWarehouse(Warehouse warehouse) {
        set(PROPERTY_WAREHOUSE, warehouse);
    }

    public BigDecimal getLitros() {
        return (BigDecimal) get(PROPERTY_LITROS);
    }

    public void setLitros(BigDecimal litros) {
        set(PROPERTY_LITROS, litros);
    }

    public Boolean isContenedor() {
        return (Boolean) get(PROPERTY_CONTENEDOR);
    }

    public void setContenedor(Boolean contenedor) {
        set(PROPERTY_CONTENEDOR, contenedor);
    }

    public PNUDR_Zona getPnudrZona() {
        return (PNUDR_Zona) get(PROPERTY_PNUDRZONA);
    }

    public void setPnudrZona(PNUDR_Zona pnudrZona) {
        set(PROPERTY_PNUDRZONA, pnudrZona);
    }

}
