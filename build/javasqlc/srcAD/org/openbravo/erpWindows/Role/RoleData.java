//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.Role;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RoleData implements FieldProvider {
static Logger log4j = Logger.getLogger(RoleData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adClientId;
  public String adClientIdr;
  public String adOrgId;
  public String adOrgIdr;
  public String name;
  public String userlevel;
  public String userlevelr;
  public String ismanual;
  public String isadvanced;
  public String description;
  public String isClientAdmin;
  public String isactive;
  public String processing;
  public String isrestrictbackend;
  public String isportal;
  public String isportaladmin;
  public String emPnudrGenRoles;
  public String amtapproval;
  public String cCurrencyId;
  public String adRoleId;
  public String orglist;
  public String clientlist;
  public String adTreeMenuId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CREATED"))
      return created;
    else if (fieldName.equalsIgnoreCase("CREATEDBYR"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("UPDATED"))
      return updated;
    else if (fieldName.equalsIgnoreCase("UPDATED_TIME_STAMP") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("UPDATEDBY"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("UPDATEDBYR"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_IDR") || fieldName.equals("adClientIdr"))
      return adClientIdr;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_IDR") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("NAME"))
      return name;
    else if (fieldName.equalsIgnoreCase("USERLEVEL"))
      return userlevel;
    else if (fieldName.equalsIgnoreCase("USERLEVELR"))
      return userlevelr;
    else if (fieldName.equalsIgnoreCase("ISMANUAL"))
      return ismanual;
    else if (fieldName.equalsIgnoreCase("ISADVANCED"))
      return isadvanced;
    else if (fieldName.equalsIgnoreCase("DESCRIPTION"))
      return description;
    else if (fieldName.equalsIgnoreCase("IS_CLIENT_ADMIN") || fieldName.equals("isClientAdmin"))
      return isClientAdmin;
    else if (fieldName.equalsIgnoreCase("ISACTIVE"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("PROCESSING"))
      return processing;
    else if (fieldName.equalsIgnoreCase("ISRESTRICTBACKEND"))
      return isrestrictbackend;
    else if (fieldName.equalsIgnoreCase("ISPORTAL"))
      return isportal;
    else if (fieldName.equalsIgnoreCase("ISPORTALADMIN"))
      return isportaladmin;
    else if (fieldName.equalsIgnoreCase("EM_PNUDR_GEN_ROLES") || fieldName.equals("emPnudrGenRoles"))
      return emPnudrGenRoles;
    else if (fieldName.equalsIgnoreCase("AMTAPPROVAL"))
      return amtapproval;
    else if (fieldName.equalsIgnoreCase("C_CURRENCY_ID") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("AD_ROLE_ID") || fieldName.equals("adRoleId"))
      return adRoleId;
    else if (fieldName.equalsIgnoreCase("ORGLIST"))
      return orglist;
    else if (fieldName.equalsIgnoreCase("CLIENTLIST"))
      return clientlist;
    else if (fieldName.equalsIgnoreCase("AD_TREE_MENU_ID") || fieldName.equals("adTreeMenuId"))
      return adTreeMenuId;
    else if (fieldName.equalsIgnoreCase("LANGUAGE"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RoleData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ad_client_id, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, ad_client_id, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RoleData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ad_client_id, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(AD_Role.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = AD_Role.CreatedBy) as CreatedByR, " +
      "        to_char(AD_Role.Updated, ?) as updated, " +
      "        to_char(AD_Role.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        AD_Role.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = AD_Role.UpdatedBy) as UpdatedByR," +
      "        AD_Role.AD_Client_ID, " +
      "(CASE WHEN AD_Role.AD_Client_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Client_IDR, " +
      "AD_Role.AD_Org_ID, " +
      "(CASE WHEN AD_Role.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "AD_Role.Name, " +
      "AD_Role.UserLevel, " +
      "(CASE WHEN AD_Role.UserLevel IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS UserLevelR, " +
      "COALESCE(AD_Role.IsManual, 'N') AS IsManual, " +
      "COALESCE(AD_Role.IsAdvanced, 'N') AS IsAdvanced, " +
      "AD_Role.Description, " +
      "COALESCE(AD_Role.IS_Client_Admin, 'N') AS IS_Client_Admin, " +
      "COALESCE(AD_Role.IsActive, 'N') AS IsActive, " +
      "AD_Role.Processing, " +
      "COALESCE(AD_Role.Isrestrictbackend, 'N') AS Isrestrictbackend, " +
      "COALESCE(AD_Role.IsPortal, 'N') AS IsPortal, " +
      "COALESCE(AD_Role.IsPortalAdmin, 'N') AS IsPortalAdmin, " +
      "AD_Role.EM_Pnudr_Gen_Roles, " +
      "AD_Role.AmtApproval, " +
      "AD_Role.C_Currency_ID, " +
      "AD_Role.AD_Role_ID, " +
      "AD_Role.OrgList, " +
      "AD_Role.ClientList, " +
      "AD_Role.AD_Tree_Menu_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM AD_Role left join (select AD_Client_ID, Name from AD_Client) table1 on (AD_Role.AD_Client_ID = table1.AD_Client_ID) left join (select AD_Org_ID, Name from AD_Org) table2 on (AD_Role.AD_Org_ID = table2.AD_Org_ID) left join ad_ref_list_v list1 on (AD_Role.UserLevel = list1.value and list1.ad_reference_id = '226' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      " AND AD_Role.ad_client_id = ?" +
      "        AND 1=1 " +
      "        AND AD_Role.AD_Role_ID = ? " +
      "        AND AD_Role.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND AD_Role.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RoleData objectRoleData = new RoleData();
        objectRoleData.created = UtilSql.getValue(result, "CREATED");
        objectRoleData.createdbyr = UtilSql.getValue(result, "CREATEDBYR");
        objectRoleData.updated = UtilSql.getValue(result, "UPDATED");
        objectRoleData.updatedTimeStamp = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
        objectRoleData.updatedby = UtilSql.getValue(result, "UPDATEDBY");
        objectRoleData.updatedbyr = UtilSql.getValue(result, "UPDATEDBYR");
        objectRoleData.adClientId = UtilSql.getValue(result, "AD_CLIENT_ID");
        objectRoleData.adClientIdr = UtilSql.getValue(result, "AD_CLIENT_IDR");
        objectRoleData.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectRoleData.adOrgIdr = UtilSql.getValue(result, "AD_ORG_IDR");
        objectRoleData.name = UtilSql.getValue(result, "NAME");
        objectRoleData.userlevel = UtilSql.getValue(result, "USERLEVEL");
        objectRoleData.userlevelr = UtilSql.getValue(result, "USERLEVELR");
        objectRoleData.ismanual = UtilSql.getValue(result, "ISMANUAL");
        objectRoleData.isadvanced = UtilSql.getValue(result, "ISADVANCED");
        objectRoleData.description = UtilSql.getValue(result, "DESCRIPTION");
        objectRoleData.isClientAdmin = UtilSql.getValue(result, "IS_CLIENT_ADMIN");
        objectRoleData.isactive = UtilSql.getValue(result, "ISACTIVE");
        objectRoleData.processing = UtilSql.getValue(result, "PROCESSING");
        objectRoleData.isrestrictbackend = UtilSql.getValue(result, "ISRESTRICTBACKEND");
        objectRoleData.isportal = UtilSql.getValue(result, "ISPORTAL");
        objectRoleData.isportaladmin = UtilSql.getValue(result, "ISPORTALADMIN");
        objectRoleData.emPnudrGenRoles = UtilSql.getValue(result, "EM_PNUDR_GEN_ROLES");
        objectRoleData.amtapproval = UtilSql.getValue(result, "AMTAPPROVAL");
        objectRoleData.cCurrencyId = UtilSql.getValue(result, "C_CURRENCY_ID");
        objectRoleData.adRoleId = UtilSql.getValue(result, "AD_ROLE_ID");
        objectRoleData.orglist = UtilSql.getValue(result, "ORGLIST");
        objectRoleData.clientlist = UtilSql.getValue(result, "CLIENTLIST");
        objectRoleData.adTreeMenuId = UtilSql.getValue(result, "AD_TREE_MENU_ID");
        objectRoleData.language = UtilSql.getValue(result, "LANGUAGE");
        objectRoleData.adUserClient = "";
        objectRoleData.adOrgClient = "";
        objectRoleData.createdby = "";
        objectRoleData.trBgcolor = "";
        objectRoleData.totalCount = "";
        objectRoleData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRoleData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RoleData objectRoleData[] = new RoleData[vector.size()];
    vector.copyInto(objectRoleData);
    return(objectRoleData);
  }

/**
Create a registry
 */
  public static RoleData[] set(String isrestrictbackend, String emPnudrGenRoles, String isportal, String cCurrencyId, String amtapproval, String isportaladmin, String adRoleId, String name, String description, String userlevel, String clientlist, String orglist, String adClientId, String adOrgId, String isadvanced, String adTreeMenuId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String processing, String isClientAdmin, String ismanual)    throws ServletException {
    RoleData objectRoleData[] = new RoleData[1];
    objectRoleData[0] = new RoleData();
    objectRoleData[0].created = "";
    objectRoleData[0].createdbyr = createdbyr;
    objectRoleData[0].updated = "";
    objectRoleData[0].updatedTimeStamp = "";
    objectRoleData[0].updatedby = updatedby;
    objectRoleData[0].updatedbyr = updatedbyr;
    objectRoleData[0].adClientId = adClientId;
    objectRoleData[0].adClientIdr = "";
    objectRoleData[0].adOrgId = adOrgId;
    objectRoleData[0].adOrgIdr = "";
    objectRoleData[0].name = name;
    objectRoleData[0].userlevel = userlevel;
    objectRoleData[0].userlevelr = "";
    objectRoleData[0].ismanual = ismanual;
    objectRoleData[0].isadvanced = isadvanced;
    objectRoleData[0].description = description;
    objectRoleData[0].isClientAdmin = isClientAdmin;
    objectRoleData[0].isactive = isactive;
    objectRoleData[0].processing = processing;
    objectRoleData[0].isrestrictbackend = isrestrictbackend;
    objectRoleData[0].isportal = isportal;
    objectRoleData[0].isportaladmin = isportaladmin;
    objectRoleData[0].emPnudrGenRoles = emPnudrGenRoles;
    objectRoleData[0].amtapproval = amtapproval;
    objectRoleData[0].cCurrencyId = cCurrencyId;
    objectRoleData[0].adRoleId = adRoleId;
    objectRoleData[0].orglist = orglist;
    objectRoleData[0].clientlist = clientlist;
    objectRoleData[0].adTreeMenuId = adTreeMenuId;
    objectRoleData[0].language = "";
    return objectRoleData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef718_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "CREATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef720_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE AD_Role" +
      "        SET AD_Client_ID = (?) , AD_Org_ID = (?) , Name = (?) , UserLevel = (?) , IsManual = (?) , IsAdvanced = (?) , Description = (?) , IS_Client_Admin = (?) , IsActive = (?) , Processing = (?) , Isrestrictbackend = (?) , IsPortal = (?) , IsPortalAdmin = (?) , EM_Pnudr_Gen_Roles = (?) , AmtApproval = TO_NUMBER(?) , C_Currency_ID = (?) , AD_Role_ID = (?) , OrgList = (?) , ClientList = (?) , AD_Tree_Menu_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE AD_Role.AD_Role_ID = ? " +
      "        AND AD_Role.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND AD_Role.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, userlevel);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ismanual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isadvanced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isClientAdmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isrestrictbackend);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isportal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isportaladmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emPnudrGenRoles);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtapproval);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adRoleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orglist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientlist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeMenuId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adRoleId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO AD_Role " +
      "        (AD_Client_ID, AD_Org_ID, Name, UserLevel, IsManual, IsAdvanced, Description, IS_Client_Admin, IsActive, Processing, Isrestrictbackend, IsPortal, IsPortalAdmin, EM_Pnudr_Gen_Roles, AmtApproval, C_Currency_ID, AD_Role_ID, OrgList, ClientList, AD_Tree_Menu_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, userlevel);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ismanual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isadvanced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isClientAdmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isrestrictbackend);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isportal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isportaladmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emPnudrGenRoles);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amtapproval);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adRoleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orglist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientlist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeMenuId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM AD_Role" +
      "        WHERE AD_Role.AD_Role_ID = ? " +
      "        AND AD_Role.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND AD_Role.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM AD_Role" +
      "         WHERE AD_Role.AD_Role_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "AD_ORG_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM AD_Role" +
      "         WHERE AD_Role.AD_Role_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
