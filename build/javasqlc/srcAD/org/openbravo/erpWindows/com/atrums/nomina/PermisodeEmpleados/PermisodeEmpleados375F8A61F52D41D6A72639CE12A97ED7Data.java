//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.PermisodeEmpleados;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data implements FieldProvider {
static Logger log4j = Logger.getLogger(PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String procesado;
  public String tipoPermiso;
  public String tipoPermisor;
  public String fechaPermiso;
  public String dias;
  public String horas;
  public String estado;
  public String estador;
  public String isactive;
  public String processed;
  public String motivoPermiso;
  public String motivoPermisor;
  public String adClientId;
  public String noPermisoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CREATED"))
      return created;
    else if (fieldName.equalsIgnoreCase("CREATEDBYR"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("UPDATED"))
      return updated;
    else if (fieldName.equalsIgnoreCase("UPDATED_TIME_STAMP") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("UPDATEDBY"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("UPDATEDBYR"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_IDR") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("C_BPARTNER_ID") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("C_BPARTNER_IDR") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("PROCESADO"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("TIPO_PERMISO") || fieldName.equals("tipoPermiso"))
      return tipoPermiso;
    else if (fieldName.equalsIgnoreCase("TIPO_PERMISOR") || fieldName.equals("tipoPermisor"))
      return tipoPermisor;
    else if (fieldName.equalsIgnoreCase("FECHA_PERMISO") || fieldName.equals("fechaPermiso"))
      return fechaPermiso;
    else if (fieldName.equalsIgnoreCase("DIAS"))
      return dias;
    else if (fieldName.equalsIgnoreCase("HORAS"))
      return horas;
    else if (fieldName.equalsIgnoreCase("ESTADO"))
      return estado;
    else if (fieldName.equalsIgnoreCase("ESTADOR"))
      return estador;
    else if (fieldName.equalsIgnoreCase("ISACTIVE"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("PROCESSED"))
      return processed;
    else if (fieldName.equalsIgnoreCase("MOTIVO_PERMISO") || fieldName.equals("motivoPermiso"))
      return motivoPermiso;
    else if (fieldName.equalsIgnoreCase("MOTIVO_PERMISOR") || fieldName.equals("motivoPermisor"))
      return motivoPermisor;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("NO_PERMISO_ID") || fieldName.equals("noPermisoId"))
      return noPermisoId;
    else if (fieldName.equalsIgnoreCase("LANGUAGE"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_permiso.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_permiso.CreatedBy) as CreatedByR, " +
      "        to_char(no_permiso.Updated, ?) as updated, " +
      "        to_char(no_permiso.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_permiso.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_permiso.UpdatedBy) as UpdatedByR," +
      "        no_permiso.AD_Org_ID, " +
      "(CASE WHEN no_permiso.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_permiso.C_Bpartner_ID, " +
      "(CASE WHEN no_permiso.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "COALESCE(no_permiso.Procesado, 'N') AS Procesado, " +
      "no_permiso.Tipo_Permiso, " +
      "(CASE WHEN no_permiso.Tipo_Permiso IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_PermisoR, " +
      "no_permiso.Fecha_Permiso, " +
      "no_permiso.Dias, " +
      "no_permiso.Horas, " +
      "no_permiso.Estado, " +
      "(CASE WHEN no_permiso.Estado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS EstadoR, " +
      "COALESCE(no_permiso.Isactive, 'N') AS Isactive, " +
      "no_permiso.Processed, " +
      "no_permiso.Motivo_Permiso, " +
      "(CASE WHEN no_permiso.Motivo_Permiso IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS Motivo_PermisoR, " +
      "no_permiso.AD_Client_ID, " +
      "no_permiso.NO_Permiso_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_permiso left join (select AD_Org_ID, Name from AD_Org) table1 on (no_permiso.AD_Org_ID = table1.AD_Org_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (no_permiso.C_Bpartner_ID = table2.C_BPartner_ID) left join ad_ref_list_v list1 on (no_permiso.Tipo_Permiso = list1.value and list1.ad_reference_id = '7FB5608D7ED64B0DA88308BE0234600A' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (no_permiso.Estado = list2.value and list2.ad_reference_id = '9C5B12FF0D424C638816FA7BC8E17B9B' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (no_permiso.Motivo_Permiso = list3.value and list3.ad_reference_id = '5BCEFDAAF41642E5BA3E334D4C810E6D' and list3.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_permiso.NO_Permiso_ID = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data = new PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data();
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.created = UtilSql.getValue(result, "CREATED");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.createdbyr = UtilSql.getValue(result, "CREATEDBYR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.updated = UtilSql.getValue(result, "UPDATED");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.updatedTimeStamp = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.updatedby = UtilSql.getValue(result, "UPDATEDBY");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.updatedbyr = UtilSql.getValue(result, "UPDATEDBYR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.adOrgIdr = UtilSql.getValue(result, "AD_ORG_IDR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.cBpartnerId = UtilSql.getValue(result, "C_BPARTNER_ID");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.cBpartnerIdr = UtilSql.getValue(result, "C_BPARTNER_IDR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.procesado = UtilSql.getValue(result, "PROCESADO");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.tipoPermiso = UtilSql.getValue(result, "TIPO_PERMISO");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.tipoPermisor = UtilSql.getValue(result, "TIPO_PERMISOR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.fechaPermiso = UtilSql.getDateValue(result, "FECHA_PERMISO", "dd-MM-yyyy");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.dias = UtilSql.getValue(result, "DIAS");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.horas = UtilSql.getValue(result, "HORAS");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.estado = UtilSql.getValue(result, "ESTADO");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.estador = UtilSql.getValue(result, "ESTADOR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.isactive = UtilSql.getValue(result, "ISACTIVE");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.processed = UtilSql.getValue(result, "PROCESSED");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.motivoPermiso = UtilSql.getValue(result, "MOTIVO_PERMISO");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.motivoPermisor = UtilSql.getValue(result, "MOTIVO_PERMISOR");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.adClientId = UtilSql.getValue(result, "AD_CLIENT_ID");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.noPermisoId = UtilSql.getValue(result, "NO_PERMISO_ID");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.language = UtilSql.getValue(result, "LANGUAGE");
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.adUserClient = "";
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.adOrgClient = "";
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.createdby = "";
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.trBgcolor = "";
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.totalCount = "";
        objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[] = new PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[vector.size()];
    vector.copyInto(objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data);
    return(objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data);
  }

/**
Create a registry
 */
  public static PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[] set(String estado, String horas, String adClientId, String tipoPermiso, String processed, String motivoPermiso, String fechaPermiso, String adOrgId, String dias, String createdby, String createdbyr, String updatedby, String updatedbyr, String cBpartnerId, String cBpartnerIdr, String isactive, String noPermisoId, String procesado)    throws ServletException {
    PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[] = new PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[1];
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0] = new PermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data();
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].created = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].createdbyr = createdbyr;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].updated = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].updatedTimeStamp = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].updatedby = updatedby;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].updatedbyr = updatedbyr;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].adOrgId = adOrgId;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].adOrgIdr = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].cBpartnerId = cBpartnerId;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].cBpartnerIdr = cBpartnerIdr;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].procesado = procesado;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].tipoPermiso = tipoPermiso;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].tipoPermisor = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].fechaPermiso = fechaPermiso;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].dias = dias;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].horas = horas;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].estado = estado;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].estador = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].isactive = isactive;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].processed = processed;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].motivoPermiso = motivoPermiso;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].motivoPermisor = "";
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].adClientId = adClientId;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].noPermisoId = noPermisoId;
    objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data[0].language = "";
    return objectPermisodeEmpleados375F8A61F52D41D6A72639CE12A97ED7Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef3DE858E5D80845BD9146262347FBD737_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "CREATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4095570FE9E54FECB4F9FAF7C2A44187_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef5F0524840C94410B8676C582C28E74A0_2(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "C_BPARTNER_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_permiso" +
      "        SET AD_Org_ID = (?) , C_Bpartner_ID = (?) , Procesado = (?) , Tipo_Permiso = (?) , Fecha_Permiso = TO_DATE(?) , Dias = TO_NUMBER(?) , Horas = TO_NUMBER(?) , Estado = (?) , Isactive = (?) , Processed = (?) , Motivo_Permiso = (?) , AD_Client_ID = (?) , NO_Permiso_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_permiso.NO_Permiso_ID = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, horas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, motivoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_permiso " +
      "        (AD_Org_ID, C_Bpartner_ID, Procesado, Tipo_Permiso, Fecha_Permiso, Dias, Horas, Estado, Isactive, Processed, Motivo_Permiso, AD_Client_ID, NO_Permiso_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, horas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, motivoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_permiso" +
      "        WHERE no_permiso.NO_Permiso_ID = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_permiso" +
      "         WHERE no_permiso.NO_Permiso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "AD_ORG_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_permiso" +
      "         WHERE no_permiso.NO_Permiso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
