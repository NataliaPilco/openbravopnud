//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.GeneradorPagosNomina;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data implements FieldProvider {
static Logger log4j = Logger.getLogger(PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String cPeriodId;
  public String cPeriodIdr;
  public String noAreaEmpresaId;
  public String noAreaEmpresaIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String generarRegistro;
  public String finFinancialAccountId;
  public String finFinancialAccountIdr;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String cGlitemId;
  public String cGlitemIdr;
  public String fechaPago;
  public String pagoCDoctypeId;
  public String pagoCDoctypeIdr;
  public String noPagoCabeceraId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CREATED"))
      return created;
    else if (fieldName.equalsIgnoreCase("CREATEDBYR"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("UPDATED"))
      return updated;
    else if (fieldName.equalsIgnoreCase("UPDATED_TIME_STAMP") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("UPDATEDBY"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("UPDATEDBYR"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_IDR") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("ISACTIVE"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("C_PERIOD_ID") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("C_PERIOD_IDR") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("NO_AREA_EMPRESA_ID") || fieldName.equals("noAreaEmpresaId"))
      return noAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("NO_AREA_EMPRESA_IDR") || fieldName.equals("noAreaEmpresaIdr"))
      return noAreaEmpresaIdr;
    else if (fieldName.equalsIgnoreCase("C_DOCTYPE_ID") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("C_DOCTYPE_IDR") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("GENERAR_REGISTRO") || fieldName.equals("generarRegistro"))
      return generarRegistro;
    else if (fieldName.equalsIgnoreCase("FIN_FINANCIAL_ACCOUNT_ID") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("FIN_FINANCIAL_ACCOUNT_IDR") || fieldName.equals("finFinancialAccountIdr"))
      return finFinancialAccountIdr;
    else if (fieldName.equalsIgnoreCase("FIN_PAYMENTMETHOD_ID") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("FIN_PAYMENTMETHOD_IDR") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("C_CURRENCY_ID") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("C_CURRENCY_IDR") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("C_GLITEM_ID") || fieldName.equals("cGlitemId"))
      return cGlitemId;
    else if (fieldName.equalsIgnoreCase("C_GLITEM_IDR") || fieldName.equals("cGlitemIdr"))
      return cGlitemIdr;
    else if (fieldName.equalsIgnoreCase("FECHA_PAGO") || fieldName.equals("fechaPago"))
      return fechaPago;
    else if (fieldName.equalsIgnoreCase("PAGO_C_DOCTYPE_ID") || fieldName.equals("pagoCDoctypeId"))
      return pagoCDoctypeId;
    else if (fieldName.equalsIgnoreCase("PAGO_C_DOCTYPE_IDR") || fieldName.equals("pagoCDoctypeIdr"))
      return pagoCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("NO_PAGO_CABECERA_ID") || fieldName.equals("noPagoCabeceraId"))
      return noPagoCabeceraId;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("LANGUAGE"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_pago_cabecera.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_pago_cabecera.CreatedBy) as CreatedByR, " +
      "        to_char(no_pago_cabecera.Updated, ?) as updated, " +
      "        to_char(no_pago_cabecera.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_pago_cabecera.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_pago_cabecera.UpdatedBy) as UpdatedByR," +
      "        no_pago_cabecera.AD_Org_ID, " +
      "(CASE WHEN no_pago_cabecera.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(no_pago_cabecera.Isactive, 'N') AS Isactive, " +
      "no_pago_cabecera.C_Period_ID, " +
      "(CASE WHEN no_pago_cabecera.C_Period_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "no_pago_cabecera.NO_Area_Empresa_ID, " +
      "(CASE WHEN no_pago_cabecera.NO_Area_Empresa_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Nombre), ''))),'') ) END) AS NO_Area_Empresa_IDR, " +
      "no_pago_cabecera.C_Doctype_ID, " +
      "(CASE WHEN no_pago_cabecera.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "no_pago_cabecera.Generar_Registro, " +
      "no_pago_cabecera.FIN_Financial_Account_ID, " +
      "(CASE WHEN no_pago_cabecera.FIN_Financial_Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.ISO_Code), ''))),'') ) END) AS FIN_Financial_Account_IDR, " +
      "no_pago_cabecera.FIN_Paymentmethod_ID, " +
      "(CASE WHEN no_pago_cabecera.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "no_pago_cabecera.C_Currency_ID, " +
      "(CASE WHEN no_pago_cabecera.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "no_pago_cabecera.C_Glitem_ID, " +
      "(CASE WHEN no_pago_cabecera.C_Glitem_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS C_Glitem_IDR, " +
      "no_pago_cabecera.Fecha_Pago, " +
      "no_pago_cabecera.Pago_C_Doctype_ID, " +
      "(CASE WHEN no_pago_cabecera.Pago_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL11.Name IS NULL THEN TO_CHAR(table11.Name) ELSE TO_CHAR(tableTRL11.Name) END)), ''))),'') ) END) AS Pago_C_Doctype_IDR, " +
      "no_pago_cabecera.NO_Pago_Cabecera_ID, " +
      "no_pago_cabecera.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_pago_cabecera left join (select AD_Org_ID, Name from AD_Org) table1 on (no_pago_cabecera.AD_Org_ID = table1.AD_Org_ID) left join (select C_Period_ID, Name from C_Period) table2 on (no_pago_cabecera.C_Period_ID =  table2.C_Period_ID) left join (select NO_Area_Empresa_ID, Nombre from NO_Area_Empresa) table3 on (no_pago_cabecera.NO_Area_Empresa_ID = table3.NO_Area_Empresa_ID) left join (select C_DocType_ID, Name from C_DocType) table4 on (no_pago_cabecera.C_Doctype_ID =  table4.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL4 on (table4.C_DocType_ID = tableTRL4.C_DocType_ID and tableTRL4.AD_Language = ?)  left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table6 on (no_pago_cabecera.FIN_Financial_Account_ID = table6.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table7 on (table6.C_Currency_ID = table7.C_Currency_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table8 on (no_pago_cabecera.FIN_Paymentmethod_ID = table8.FIN_Paymentmethod_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table9 on (no_pago_cabecera.C_Currency_ID = table9.C_Currency_ID) left join (select C_Glitem_ID, Name from C_Glitem) table10 on (no_pago_cabecera.C_Glitem_ID =  table10.C_Glitem_ID) left join (select C_DocType_ID, Name from C_DocType) table11 on (no_pago_cabecera.Pago_C_Doctype_ID =  table11.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL11 on (table11.C_DocType_ID = tableTRL11.C_DocType_ID and tableTRL11.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_pago_cabecera.NO_Pago_Cabecera_ID = ? " +
      "        AND no_pago_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_pago_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data = new PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data();
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.created = UtilSql.getValue(result, "CREATED");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.createdbyr = UtilSql.getValue(result, "CREATEDBYR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.updated = UtilSql.getValue(result, "UPDATED");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.updatedTimeStamp = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.updatedby = UtilSql.getValue(result, "UPDATEDBY");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.updatedbyr = UtilSql.getValue(result, "UPDATEDBYR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.adOrgIdr = UtilSql.getValue(result, "AD_ORG_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.isactive = UtilSql.getValue(result, "ISACTIVE");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cPeriodId = UtilSql.getValue(result, "C_PERIOD_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cPeriodIdr = UtilSql.getValue(result, "C_PERIOD_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.noAreaEmpresaId = UtilSql.getValue(result, "NO_AREA_EMPRESA_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.noAreaEmpresaIdr = UtilSql.getValue(result, "NO_AREA_EMPRESA_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cDoctypeId = UtilSql.getValue(result, "C_DOCTYPE_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cDoctypeIdr = UtilSql.getValue(result, "C_DOCTYPE_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.generarRegistro = UtilSql.getValue(result, "GENERAR_REGISTRO");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.finFinancialAccountId = UtilSql.getValue(result, "FIN_FINANCIAL_ACCOUNT_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.finFinancialAccountIdr = UtilSql.getValue(result, "FIN_FINANCIAL_ACCOUNT_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.finPaymentmethodId = UtilSql.getValue(result, "FIN_PAYMENTMETHOD_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.finPaymentmethodIdr = UtilSql.getValue(result, "FIN_PAYMENTMETHOD_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cCurrencyId = UtilSql.getValue(result, "C_CURRENCY_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cCurrencyIdr = UtilSql.getValue(result, "C_CURRENCY_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cGlitemId = UtilSql.getValue(result, "C_GLITEM_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.cGlitemIdr = UtilSql.getValue(result, "C_GLITEM_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.fechaPago = UtilSql.getDateValue(result, "FECHA_PAGO", "dd-MM-yyyy");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.pagoCDoctypeId = UtilSql.getValue(result, "PAGO_C_DOCTYPE_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.pagoCDoctypeIdr = UtilSql.getValue(result, "PAGO_C_DOCTYPE_IDR");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.noPagoCabeceraId = UtilSql.getValue(result, "NO_PAGO_CABECERA_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.adClientId = UtilSql.getValue(result, "AD_CLIENT_ID");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.language = UtilSql.getValue(result, "LANGUAGE");
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.adUserClient = "";
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.adOrgClient = "";
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.createdby = "";
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.trBgcolor = "";
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.totalCount = "";
        objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[] = new PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[vector.size()];
    vector.copyInto(objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data);
    return(objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data);
  }

/**
Create a registry
 */
  public static PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[] set(String cGlitemId, String pagoCDoctypeId, String cPeriodId, String finPaymentmethodId, String fechaPago, String finFinancialAccountId, String updatedby, String updatedbyr, String isactive, String adClientId, String noAreaEmpresaId, String cDoctypeId, String generarRegistro, String cCurrencyId, String createdby, String createdbyr, String noPagoCabeceraId, String adOrgId)    throws ServletException {
    PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[] = new PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[1];
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0] = new PagoNominaCabecera424295FE162A4CF88B993BD228C47679Data();
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].created = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].createdbyr = createdbyr;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].updated = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].updatedTimeStamp = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].updatedby = updatedby;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].updatedbyr = updatedbyr;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].adOrgId = adOrgId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].adOrgIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].isactive = isactive;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cPeriodId = cPeriodId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cPeriodIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].noAreaEmpresaId = noAreaEmpresaId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].noAreaEmpresaIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cDoctypeId = cDoctypeId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cDoctypeIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].generarRegistro = generarRegistro;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].finFinancialAccountId = finFinancialAccountId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].finFinancialAccountIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].finPaymentmethodId = finPaymentmethodId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].finPaymentmethodIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cCurrencyId = cCurrencyId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cCurrencyIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cGlitemId = cGlitemId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].cGlitemIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].fechaPago = fechaPago;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].pagoCDoctypeId = pagoCDoctypeId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].pagoCDoctypeIdr = "";
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].noPagoCabeceraId = noPagoCabeceraId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].adClientId = adClientId;
    objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data[0].language = "";
    return objectPagoNominaCabecera424295FE162A4CF88B993BD228C47679Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDefCD014305841349FA8DAD07A5AFAF0944(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='0') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "DEFAULTVALUE");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefF93FB5F7508946B4BBECAB20D41294B8_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef618E58A86E0D4E6E87F07302539C06E5_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "CREATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_pago_cabecera" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , C_Period_ID = (?) , NO_Area_Empresa_ID = (?) , C_Doctype_ID = (?) , Generar_Registro = (?) , FIN_Financial_Account_ID = (?) , FIN_Paymentmethod_ID = (?) , C_Currency_ID = (?) , C_Glitem_ID = (?) , Fecha_Pago = TO_DATE(?) , Pago_C_Doctype_ID = (?) , NO_Pago_Cabecera_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_pago_cabecera.NO_Pago_Cabecera_ID = ? " +
      "        AND no_pago_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_pago_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarRegistro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPagoCabeceraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPagoCabeceraId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_pago_cabecera " +
      "        (AD_Org_ID, Isactive, C_Period_ID, NO_Area_Empresa_ID, C_Doctype_ID, Generar_Registro, FIN_Financial_Account_ID, FIN_Paymentmethod_ID, C_Currency_ID, C_Glitem_ID, Fecha_Pago, Pago_C_Doctype_ID, NO_Pago_Cabecera_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarRegistro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cGlitemId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagoCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPagoCabeceraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_pago_cabecera" +
      "        WHERE no_pago_cabecera.NO_Pago_Cabecera_ID = ? " +
      "        AND no_pago_cabecera.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_pago_cabecera.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_pago_cabecera" +
      "         WHERE no_pago_cabecera.NO_Pago_Cabecera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "AD_ORG_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_pago_cabecera" +
      "         WHERE no_pago_cabecera.NO_Pago_Cabecera_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
