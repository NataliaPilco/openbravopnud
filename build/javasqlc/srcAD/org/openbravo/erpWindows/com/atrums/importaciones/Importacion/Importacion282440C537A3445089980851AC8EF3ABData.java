//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.importaciones.Importacion;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class Importacion282440C537A3445089980851AC8EF3ABData implements FieldProvider {
static Logger log4j = Logger.getLogger(Importacion282440C537A3445089980851AC8EF3ABData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String isactive;
  public String documentno;
  public String name;
  public String descripcion;
  public String fechaPago;
  public String fechaEta;
  public String fechaEtd;
  public String total;
  public String docstatus;
  public String docactionim;
  public String docactionimBtn;
  public String procesarimp;
  public String adClientId;
  public String atimpImportacionId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CREATED"))
      return created;
    else if (fieldName.equalsIgnoreCase("CREATEDBYR"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("UPDATED"))
      return updated;
    else if (fieldName.equalsIgnoreCase("UPDATED_TIME_STAMP") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("UPDATEDBY"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("UPDATEDBYR"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_IDR") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("ISACTIVE"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("DOCUMENTNO"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("NAME"))
      return name;
    else if (fieldName.equalsIgnoreCase("DESCRIPCION"))
      return descripcion;
    else if (fieldName.equalsIgnoreCase("FECHA_PAGO") || fieldName.equals("fechaPago"))
      return fechaPago;
    else if (fieldName.equalsIgnoreCase("FECHA_ETA") || fieldName.equals("fechaEta"))
      return fechaEta;
    else if (fieldName.equalsIgnoreCase("FECHA_ETD") || fieldName.equals("fechaEtd"))
      return fechaEtd;
    else if (fieldName.equalsIgnoreCase("TOTAL"))
      return total;
    else if (fieldName.equalsIgnoreCase("DOCSTATUS"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("DOCACTIONIM"))
      return docactionim;
    else if (fieldName.equalsIgnoreCase("DOCACTIONIM_BTN") || fieldName.equals("docactionimBtn"))
      return docactionimBtn;
    else if (fieldName.equalsIgnoreCase("PROCESARIMP"))
      return procesarimp;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ATIMP_IMPORTACION_ID") || fieldName.equals("atimpImportacionId"))
      return atimpImportacionId;
    else if (fieldName.equalsIgnoreCase("LANGUAGE"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Importacion282440C537A3445089980851AC8EF3ABData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Importacion282440C537A3445089980851AC8EF3ABData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atimp_importacion.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atimp_importacion.CreatedBy) as CreatedByR, " +
      "        to_char(atimp_importacion.Updated, ?) as updated, " +
      "        to_char(atimp_importacion.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atimp_importacion.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atimp_importacion.UpdatedBy) as UpdatedByR," +
      "        atimp_importacion.AD_Org_ID, " +
      "(CASE WHEN atimp_importacion.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "COALESCE(atimp_importacion.Isactive, 'N') AS Isactive, " +
      "atimp_importacion.Documentno, " +
      "atimp_importacion.Name, " +
      "atimp_importacion.Descripcion, " +
      "atimp_importacion.Fecha_Pago, " +
      "atimp_importacion.Fecha_Eta, " +
      "atimp_importacion.Fecha_Etd, " +
      "atimp_importacion.Total, " +
      "atimp_importacion.Docstatus, " +
      "atimp_importacion.Docactionim, " +
      "list1.name as Docactionim_BTN, " +
      "atimp_importacion.Procesarimp, " +
      "atimp_importacion.AD_Client_ID, " +
      "atimp_importacion.Atimp_Importacion_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atimp_importacion left join (select AD_Org_ID, Name from AD_Org) table1 on (atimp_importacion.AD_Org_ID = table1.AD_Org_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = 'B6C81730BB314B49BC26C0C3636F5853' and list1.ad_language = ?  AND atimp_importacion.Docactionim = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atimp_importacion.Atimp_Importacion_ID = ? " +
      "        AND atimp_importacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atimp_importacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Importacion282440C537A3445089980851AC8EF3ABData objectImportacion282440C537A3445089980851AC8EF3ABData = new Importacion282440C537A3445089980851AC8EF3ABData();
        objectImportacion282440C537A3445089980851AC8EF3ABData.created = UtilSql.getValue(result, "CREATED");
        objectImportacion282440C537A3445089980851AC8EF3ABData.createdbyr = UtilSql.getValue(result, "CREATEDBYR");
        objectImportacion282440C537A3445089980851AC8EF3ABData.updated = UtilSql.getValue(result, "UPDATED");
        objectImportacion282440C537A3445089980851AC8EF3ABData.updatedTimeStamp = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
        objectImportacion282440C537A3445089980851AC8EF3ABData.updatedby = UtilSql.getValue(result, "UPDATEDBY");
        objectImportacion282440C537A3445089980851AC8EF3ABData.updatedbyr = UtilSql.getValue(result, "UPDATEDBYR");
        objectImportacion282440C537A3445089980851AC8EF3ABData.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectImportacion282440C537A3445089980851AC8EF3ABData.adOrgIdr = UtilSql.getValue(result, "AD_ORG_IDR");
        objectImportacion282440C537A3445089980851AC8EF3ABData.isactive = UtilSql.getValue(result, "ISACTIVE");
        objectImportacion282440C537A3445089980851AC8EF3ABData.documentno = UtilSql.getValue(result, "DOCUMENTNO");
        objectImportacion282440C537A3445089980851AC8EF3ABData.name = UtilSql.getValue(result, "NAME");
        objectImportacion282440C537A3445089980851AC8EF3ABData.descripcion = UtilSql.getValue(result, "DESCRIPCION");
        objectImportacion282440C537A3445089980851AC8EF3ABData.fechaPago = UtilSql.getDateValue(result, "FECHA_PAGO", "dd-MM-yyyy");
        objectImportacion282440C537A3445089980851AC8EF3ABData.fechaEta = UtilSql.getDateValue(result, "FECHA_ETA", "dd-MM-yyyy");
        objectImportacion282440C537A3445089980851AC8EF3ABData.fechaEtd = UtilSql.getDateValue(result, "FECHA_ETD", "dd-MM-yyyy");
        objectImportacion282440C537A3445089980851AC8EF3ABData.total = UtilSql.getValue(result, "TOTAL");
        objectImportacion282440C537A3445089980851AC8EF3ABData.docstatus = UtilSql.getValue(result, "DOCSTATUS");
        objectImportacion282440C537A3445089980851AC8EF3ABData.docactionim = UtilSql.getValue(result, "DOCACTIONIM");
        objectImportacion282440C537A3445089980851AC8EF3ABData.docactionimBtn = UtilSql.getValue(result, "DOCACTIONIM_BTN");
        objectImportacion282440C537A3445089980851AC8EF3ABData.procesarimp = UtilSql.getValue(result, "PROCESARIMP");
        objectImportacion282440C537A3445089980851AC8EF3ABData.adClientId = UtilSql.getValue(result, "AD_CLIENT_ID");
        objectImportacion282440C537A3445089980851AC8EF3ABData.atimpImportacionId = UtilSql.getValue(result, "ATIMP_IMPORTACION_ID");
        objectImportacion282440C537A3445089980851AC8EF3ABData.language = UtilSql.getValue(result, "LANGUAGE");
        objectImportacion282440C537A3445089980851AC8EF3ABData.adUserClient = "";
        objectImportacion282440C537A3445089980851AC8EF3ABData.adOrgClient = "";
        objectImportacion282440C537A3445089980851AC8EF3ABData.createdby = "";
        objectImportacion282440C537A3445089980851AC8EF3ABData.trBgcolor = "";
        objectImportacion282440C537A3445089980851AC8EF3ABData.totalCount = "";
        objectImportacion282440C537A3445089980851AC8EF3ABData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectImportacion282440C537A3445089980851AC8EF3ABData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Importacion282440C537A3445089980851AC8EF3ABData objectImportacion282440C537A3445089980851AC8EF3ABData[] = new Importacion282440C537A3445089980851AC8EF3ABData[vector.size()];
    vector.copyInto(objectImportacion282440C537A3445089980851AC8EF3ABData);
    return(objectImportacion282440C537A3445089980851AC8EF3ABData);
  }

/**
Create a registry
 */
  public static Importacion282440C537A3445089980851AC8EF3ABData[] set(String documentno, String adOrgId, String docstatus, String docactionim, String docactionimBtn, String fechaEta, String fechaEtd, String updatedby, String updatedbyr, String descripcion, String name, String createdby, String createdbyr, String atimpImportacionId, String adClientId, String procesarimp, String fechaPago, String total, String isactive)    throws ServletException {
    Importacion282440C537A3445089980851AC8EF3ABData objectImportacion282440C537A3445089980851AC8EF3ABData[] = new Importacion282440C537A3445089980851AC8EF3ABData[1];
    objectImportacion282440C537A3445089980851AC8EF3ABData[0] = new Importacion282440C537A3445089980851AC8EF3ABData();
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].created = "";
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].createdbyr = createdbyr;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].updated = "";
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].updatedTimeStamp = "";
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].updatedby = updatedby;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].updatedbyr = updatedbyr;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].adOrgId = adOrgId;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].adOrgIdr = "";
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].isactive = isactive;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].documentno = documentno;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].name = name;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].descripcion = descripcion;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].fechaPago = fechaPago;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].fechaEta = fechaEta;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].fechaEtd = fechaEtd;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].total = total;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].docstatus = docstatus;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].docactionim = docactionim;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].docactionimBtn = docactionimBtn;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].procesarimp = procesarimp;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].adClientId = adClientId;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].atimpImportacionId = atimpImportacionId;
    objectImportacion282440C537A3445089980851AC8EF3ABData[0].language = "";
    return objectImportacion282440C537A3445089980851AC8EF3ABData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefF7F3C4C15D8047DA993E9FF3AFEA6919_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef1EFF7F7EEF9A45508F01D5A32F3FA172_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "CREATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atimp_importacion" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , Documentno = (?) , Name = (?) , Descripcion = (?) , Fecha_Pago = TO_DATE(?) , Fecha_Eta = TO_DATE(?) , Fecha_Etd = TO_DATE(?) , Total = TO_NUMBER(?) , Docstatus = (?) , Docactionim = (?) , Procesarimp = (?) , AD_Client_ID = (?) , Atimp_Importacion_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atimp_importacion.Atimp_Importacion_ID = ? " +
      "        AND atimp_importacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atimp_importacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEtd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionim);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarimp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atimpImportacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atimpImportacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atimp_importacion " +
      "        (AD_Org_ID, Isactive, Documentno, Name, Descripcion, Fecha_Pago, Fecha_Eta, Fecha_Etd, Total, Docstatus, Docactionim, Procesarimp, AD_Client_ID, Atimp_Importacion_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descripcion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEtd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, total);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionim);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarimp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atimpImportacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atimp_importacion" +
      "        WHERE atimp_importacion.Atimp_Importacion_ID = ? " +
      "        AND atimp_importacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atimp_importacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atimp_importacion" +
      "         WHERE atimp_importacion.Atimp_Importacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "AD_ORG_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atimp_importacion" +
      "         WHERE atimp_importacion.Atimp_Importacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
