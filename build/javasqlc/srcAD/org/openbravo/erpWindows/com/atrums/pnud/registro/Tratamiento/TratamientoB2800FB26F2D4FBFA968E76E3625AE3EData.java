//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.pnud.registro.Tratamiento;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData implements FieldProvider {
static Logger log4j = Logger.getLogger(TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String cBpartnerId;
  public String pnudrTratamientoId;
  public String fecha;
  public String equSolAce;
  public String equSolAcer;
  public String btnProcesar;
  public String adClientId;
  public String adOrgId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CREATED"))
      return created;
    else if (fieldName.equalsIgnoreCase("CREATEDBYR"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("UPDATED"))
      return updated;
    else if (fieldName.equalsIgnoreCase("UPDATED_TIME_STAMP") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("UPDATEDBY"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("UPDATEDBYR"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("C_BPARTNER_ID") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("PNUDR_TRATAMIENTO_ID") || fieldName.equals("pnudrTratamientoId"))
      return pnudrTratamientoId;
    else if (fieldName.equalsIgnoreCase("FECHA"))
      return fecha;
    else if (fieldName.equalsIgnoreCase("EQU_SOL_ACE") || fieldName.equals("equSolAce"))
      return equSolAce;
    else if (fieldName.equalsIgnoreCase("EQU_SOL_ACER") || fieldName.equals("equSolAcer"))
      return equSolAcer;
    else if (fieldName.equalsIgnoreCase("BTN_PROCESAR") || fieldName.equals("btnProcesar"))
      return btnProcesar;
    else if (fieldName.equalsIgnoreCase("AD_CLIENT_ID") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("LANGUAGE"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(PNUDR_Tratamiento.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = PNUDR_Tratamiento.CreatedBy) as CreatedByR, " +
      "        to_char(PNUDR_Tratamiento.Updated, ?) as updated, " +
      "        to_char(PNUDR_Tratamiento.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        PNUDR_Tratamiento.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = PNUDR_Tratamiento.UpdatedBy) as UpdatedByR," +
      "        PNUDR_Tratamiento.C_Bpartner_ID, " +
      "PNUDR_Tratamiento.Pnudr_Tratamiento_ID, " +
      "PNUDR_Tratamiento.Fecha, " +
      "PNUDR_Tratamiento.EQU_Sol_Ace, " +
      "(CASE WHEN PNUDR_Tratamiento.EQU_Sol_Ace IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EQU_Sol_AceR, " +
      "PNUDR_Tratamiento.BTN_Procesar, " +
      "PNUDR_Tratamiento.AD_Client_ID, " +
      "PNUDR_Tratamiento.AD_Org_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM PNUDR_Tratamiento left join ad_ref_list_v list1 on (PNUDR_Tratamiento.EQU_Sol_Ace = list1.value and list1.ad_reference_id = '4F4F7A3864814641B92E5D40865573EA' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND PNUDR_Tratamiento.Pnudr_Tratamiento_ID = ? " +
      "        AND PNUDR_Tratamiento.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND PNUDR_Tratamiento.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData = new TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData();
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.created = UtilSql.getValue(result, "CREATED");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.createdbyr = UtilSql.getValue(result, "CREATEDBYR");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.updated = UtilSql.getValue(result, "UPDATED");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.updatedTimeStamp = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.updatedby = UtilSql.getValue(result, "UPDATEDBY");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.updatedbyr = UtilSql.getValue(result, "UPDATEDBYR");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.cBpartnerId = UtilSql.getValue(result, "C_BPARTNER_ID");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.pnudrTratamientoId = UtilSql.getValue(result, "PNUDR_TRATAMIENTO_ID");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.fecha = UtilSql.getDateValue(result, "FECHA", "dd-MM-yyyy");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.equSolAce = UtilSql.getValue(result, "EQU_SOL_ACE");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.equSolAcer = UtilSql.getValue(result, "EQU_SOL_ACER");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.btnProcesar = UtilSql.getValue(result, "BTN_PROCESAR");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.adClientId = UtilSql.getValue(result, "AD_CLIENT_ID");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.language = UtilSql.getValue(result, "LANGUAGE");
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.adUserClient = "";
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.adOrgClient = "";
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.createdby = "";
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.trBgcolor = "";
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.totalCount = "";
        objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[] = new TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[vector.size()];
    vector.copyInto(objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData);
    return(objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData);
  }

/**
Create a registry
 */
  public static TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[] set(String updatedby, String updatedbyr, String cBpartnerId, String btnProcesar, String adClientId, String equSolAce, String pnudrTratamientoId, String createdby, String createdbyr, String adOrgId, String fecha)    throws ServletException {
    TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[] = new TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[1];
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0] = new TratamientoB2800FB26F2D4FBFA968E76E3625AE3EData();
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].created = "";
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].createdbyr = createdbyr;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].updated = "";
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].updatedTimeStamp = "";
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].updatedby = updatedby;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].updatedbyr = updatedbyr;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].cBpartnerId = cBpartnerId;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].pnudrTratamientoId = pnudrTratamientoId;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].fecha = fecha;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].equSolAce = equSolAce;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].equSolAcer = "";
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].btnProcesar = btnProcesar;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].adClientId = adClientId;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].adOrgId = adOrgId;
    objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData[0].language = "";
    return objectTratamientoB2800FB26F2D4FBFA968E76E3625AE3EData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefB26C5C5F3A5D4D81B43A28B61626B2FC_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2C26ACC6EC6644AA9C11374D6EDD0626_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "CREATEDBY");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE PNUDR_Tratamiento" +
      "        SET C_Bpartner_ID = (?) , Pnudr_Tratamiento_ID = (?) , Fecha = TO_DATE(?) , EQU_Sol_Ace = (?) , BTN_Procesar = (?) , AD_Client_ID = (?) , AD_Org_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE PNUDR_Tratamiento.Pnudr_Tratamiento_ID = ? " +
      "        AND PNUDR_Tratamiento.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND PNUDR_Tratamiento.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrTratamientoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, equSolAce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrTratamientoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO PNUDR_Tratamiento " +
      "        (C_Bpartner_ID, Pnudr_Tratamiento_ID, Fecha, EQU_Sol_Ace, BTN_Procesar, AD_Client_ID, AD_Org_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_DATE(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrTratamientoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, equSolAce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM PNUDR_Tratamiento" +
      "        WHERE PNUDR_Tratamiento.Pnudr_Tratamiento_ID = ? " +
      "        AND PNUDR_Tratamiento.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND PNUDR_Tratamiento.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM PNUDR_Tratamiento" +
      "         WHERE PNUDR_Tratamiento.Pnudr_Tratamiento_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "AD_ORG_ID");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM PNUDR_Tratamiento" +
      "         WHERE PNUDR_Tratamiento.Pnudr_Tratamiento_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "UPDATED_TIME_STAMP");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
