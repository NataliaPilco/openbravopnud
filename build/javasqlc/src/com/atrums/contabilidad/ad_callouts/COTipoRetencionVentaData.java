//Sqlc generated V1.O00-1
package com.atrums.contabilidad.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class COTipoRetencionVentaData implements FieldProvider {
static Logger log4j = Logger.getLogger(COTipoRetencionVentaData.class);
  private String InitRecordNumber="0";
  public String coBpRetencionVentaId;
  public String descripcion;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CO_BP_RETENCION_VENTA_ID") || fieldName.equals("coBpRetencionVentaId"))
      return coBpRetencionVentaId;
    else if (fieldName.equalsIgnoreCase("DESCRIPCION"))
      return descripcion;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static COTipoRetencionVentaData[] select(ConnectionProvider connectionProvider, String cTipo, String coRetencionVentaId)    throws ServletException {
    return select(connectionProvider, cTipo, coRetencionVentaId, 0, 0);
  }

  public static COTipoRetencionVentaData[] select(ConnectionProvider connectionProvider, String cTipo, String coRetencionVentaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            select bprv.co_bp_retencion_venta_id, bprv.descripcion" +
      "              from co_retencion_venta rv, " +
      "                   c_invoice i, " +
      "                   co_bp_retencion_venta bprv, " +
      "                   co_tipo_retencion tr" +
      "             where i.c_invoice_id = rv.c_invoice_id" +
      "               and bprv.c_bpartner_id = i.c_bpartner_id" +
      "               and bprv.co_tipo_retencion_id = tr.co_tipo_retencion_id" +
      "               and tr.tipo = ?" +
      "               and rv.co_retencion_venta_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        COTipoRetencionVentaData objectCOTipoRetencionVentaData = new COTipoRetencionVentaData();
        objectCOTipoRetencionVentaData.coBpRetencionVentaId = UtilSql.getValue(result, "CO_BP_RETENCION_VENTA_ID");
        objectCOTipoRetencionVentaData.descripcion = UtilSql.getValue(result, "DESCRIPCION");
        objectCOTipoRetencionVentaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCOTipoRetencionVentaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    COTipoRetencionVentaData objectCOTipoRetencionVentaData[] = new COTipoRetencionVentaData[vector.size()];
    vector.copyInto(objectCOTipoRetencionVentaData);
    return(objectCOTipoRetencionVentaData);
  }
}
