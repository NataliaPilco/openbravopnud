//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PNUDRCentroAlmData implements FieldProvider {
static Logger log4j = Logger.getLogger(PNUDRCentroAlmData.class);
  private String InitRecordNumber="0";
  public String longitud;
  public String latitud;
  public String zona;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("LONGITUD"))
      return longitud;
    else if (fieldName.equalsIgnoreCase("LATITUD"))
      return latitud;
    else if (fieldName.equalsIgnoreCase("ZONA"))
      return zona;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PNUDRCentroAlmData[] select(ConnectionProvider connectionProvider, String m_warehouse_id)    throws ServletException {
    return select(connectionProvider, m_warehouse_id, 0, 0);
  }

  public static PNUDRCentroAlmData[] select(ConnectionProvider connectionProvider, String m_warehouse_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	SELECT mw.em_pnuda_longitud AS longitud, mw.em_pnuda_latitud AS latitud, mw.em_pnudr_zona_id as zona FROM m_warehouse mw" +
      "	WHERE mw.m_warehouse_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, m_warehouse_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PNUDRCentroAlmData objectPNUDRCentroAlmData = new PNUDRCentroAlmData();
        objectPNUDRCentroAlmData.longitud = UtilSql.getValue(result, "LONGITUD");
        objectPNUDRCentroAlmData.latitud = UtilSql.getValue(result, "LATITUD");
        objectPNUDRCentroAlmData.zona = UtilSql.getValue(result, "ZONA");
        objectPNUDRCentroAlmData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPNUDRCentroAlmData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PNUDRCentroAlmData objectPNUDRCentroAlmData[] = new PNUDRCentroAlmData[vector.size()];
    vector.copyInto(objectPNUDRCentroAlmData);
    return(objectPNUDRCentroAlmData);
  }
}
