//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PNUDRContenedorData implements FieldProvider {
static Logger log4j = Logger.getLogger(PNUDRContenedorData.class);
  private String InitRecordNumber="0";
  public String capacidadLt;
  public String capacidadGl;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("CAPACIDAD_LT") || fieldName.equals("capacidadLt"))
      return capacidadLt;
    else if (fieldName.equalsIgnoreCase("CAPACIDAD_GL") || fieldName.equals("capacidadGl"))
      return capacidadGl;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PNUDRContenedorData[] select(ConnectionProvider connectionProvider, String pnudrconfgequipoid)    throws ServletException {
    return select(connectionProvider, pnudrconfgequipoid, 0, 0);
  }

  public static PNUDRContenedorData[] select(ConnectionProvider connectionProvider, String pnudrconfgequipoid, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	SELECT pc.capacidad_lt, pc.capacidad_gl" +
      "	FROM pnudr_confg_equipo pc" +
      "	WHERE pc.pnudr_confg_equipo_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrconfgequipoid);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PNUDRContenedorData objectPNUDRContenedorData = new PNUDRContenedorData();
        objectPNUDRContenedorData.capacidadLt = UtilSql.getValue(result, "CAPACIDAD_LT");
        objectPNUDRContenedorData.capacidadGl = UtilSql.getValue(result, "CAPACIDAD_GL");
        objectPNUDRContenedorData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPNUDRContenedorData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PNUDRContenedorData objectPNUDRContenedorData[] = new PNUDRContenedorData[vector.size()];
    vector.copyInto(objectPNUDRContenedorData);
    return(objectPNUDRContenedorData);
  }
}
