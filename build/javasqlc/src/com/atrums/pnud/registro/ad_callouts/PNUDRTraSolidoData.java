//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PNUDRTraSolidoData implements FieldProvider {
static Logger log4j = Logger.getLogger(PNUDRTraSolidoData.class);
  private String InitRecordNumber="0";
  public String nroSerie;
  public String tipo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("NRO_SERIE") || fieldName.equals("nroSerie"))
      return nroSerie;
    else if (fieldName.equalsIgnoreCase("TIPO"))
      return tipo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PNUDRTraSolidoData[] select(ConnectionProvider connectionProvider, String pnudr_reg_solido_id)    throws ServletException {
    return select(connectionProvider, pnudr_reg_solido_id, 0, 0);
  }

  public static PNUDRTraSolidoData[] select(ConnectionProvider connectionProvider, String pnudr_reg_solido_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	SELECT prs.nro_serie, prs.pnudr_confg_equipo_id as tipo" +
      "	FROM pnudr_reg_solido prs" +
      "	WHERE prs.pnudr_reg_solido_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudr_reg_solido_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PNUDRTraSolidoData objectPNUDRTraSolidoData = new PNUDRTraSolidoData();
        objectPNUDRTraSolidoData.nroSerie = UtilSql.getValue(result, "NRO_SERIE");
        objectPNUDRTraSolidoData.tipo = UtilSql.getValue(result, "TIPO");
        objectPNUDRTraSolidoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPNUDRTraSolidoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PNUDRTraSolidoData objectPNUDRTraSolidoData[] = new PNUDRTraSolidoData[vector.size()];
    vector.copyInto(objectPNUDRTraSolidoData);
    return(objectPNUDRTraSolidoData);
  }
}
