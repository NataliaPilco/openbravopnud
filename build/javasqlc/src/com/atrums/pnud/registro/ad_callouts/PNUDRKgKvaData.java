//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PNUDRKgKvaData implements FieldProvider {
static Logger log4j = Logger.getLogger(PNUDRKgKvaData.class);
  private String InitRecordNumber="0";
  public String kva;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("KVA"))
      return kva;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PNUDRKgKvaData[] select(ConnectionProvider connectionProvider, String pnudrregequipoid)    throws ServletException {
    return select(connectionProvider, pnudrregequipoid, 0, 0);
  }

  public static PNUDRKgKvaData[] select(ConnectionProvider connectionProvider, String pnudrregequipoid, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	SELECT re.kva" +
      "	FROM pnudr_reg_equipo re" +
      "	WHERE re.pnudr_reg_equipo_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrregequipoid);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PNUDRKgKvaData objectPNUDRKgKvaData = new PNUDRKgKvaData();
        objectPNUDRKgKvaData.kva = UtilSql.getValue(result, "KVA");
        objectPNUDRKgKvaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPNUDRKgKvaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PNUDRKgKvaData objectPNUDRKgKvaData[] = new PNUDRKgKvaData[vector.size()];
    vector.copyInto(objectPNUDRKgKvaData);
    return(objectPNUDRKgKvaData);
  }
}
