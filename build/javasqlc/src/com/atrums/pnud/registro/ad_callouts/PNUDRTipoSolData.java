//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PNUDRTipoSolData implements FieldProvider {
static Logger log4j = Logger.getLogger(PNUDRTipoSolData.class);
  private String InitRecordNumber="0";
  public String pesoMaximo;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("PESO_MAXIMO") || fieldName.equals("pesoMaximo"))
      return pesoMaximo;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PNUDRTipoSolData[] select(ConnectionProvider connectionProvider, String pnudrconfgequipoid)    throws ServletException {
    return select(connectionProvider, pnudrconfgequipoid, 0, 0);
  }

  public static PNUDRTipoSolData[] select(ConnectionProvider connectionProvider, String pnudrconfgequipoid, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	SELECT pc.peso_maximo" +
      "	FROM pnudr_confg_equipo pc" +
      "	WHERE pc.pnudr_confg_equipo_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pnudrconfgequipoid);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PNUDRTipoSolData objectPNUDRTipoSolData = new PNUDRTipoSolData();
        objectPNUDRTipoSolData.pesoMaximo = UtilSql.getValue(result, "PESO_MAXIMO");
        objectPNUDRTipoSolData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPNUDRTipoSolData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PNUDRTipoSolData objectPNUDRTipoSolData[] = new PNUDRTipoSolData[vector.size()];
    vector.copyInto(objectPNUDRTipoSolData);
    return(objectPNUDRTipoSolData);
  }
}
