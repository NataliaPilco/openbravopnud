//Sqlc generated V1.O00-1
package com.atrums.pnud.registro.erpCommon.info;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class LocationParroquiaData implements FieldProvider {
static Logger log4j = Logger.getLogger(LocationParroquiaData.class);
  private String InitRecordNumber="0";
  public String id;
  public String name;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("ID"))
      return id;
    else if (fieldName.equalsIgnoreCase("NAME"))
      return name;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static LocationParroquiaData[] select(ConnectionProvider connectionProvider, String cCityId)    throws ServletException {
    return select(connectionProvider, cCityId, 0, 0);
  }

  public static LocationParroquiaData[] select(ConnectionProvider connectionProvider, String cCityId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        Select pnudr_parroquias_id as ID, name From pnudr_parroquias " +
      "		Where pnudr_parroquias.c_city_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCityId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LocationParroquiaData objectLocationParroquiaData = new LocationParroquiaData();
        objectLocationParroquiaData.id = UtilSql.getValue(result, "ID");
        objectLocationParroquiaData.name = UtilSql.getValue(result, "NAME");
        objectLocationParroquiaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLocationParroquiaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LocationParroquiaData objectLocationParroquiaData[] = new LocationParroquiaData[vector.size()];
    vector.copyInto(objectLocationParroquiaData);
    return(objectLocationParroquiaData);
  }
}
