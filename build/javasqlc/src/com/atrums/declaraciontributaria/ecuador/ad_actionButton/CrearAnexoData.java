//Sqlc generated V1.O00-1
package com.atrums.declaraciontributaria.ecuador.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class CrearAnexoData implements FieldProvider {
static Logger log4j = Logger.getLogger(CrearAnexoData.class);
  private String InitRecordNumber="0";
  public String nombreXml;
  public String nombreArchivo;
  public String tipoInformacion;
  public String procedimiento;
  public String startdate;
  public String enddate;
  public String adOrgId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("NOMBRE_XML") || fieldName.equals("nombreXml"))
      return nombreXml;
    else if (fieldName.equalsIgnoreCase("NOMBRE_ARCHIVO") || fieldName.equals("nombreArchivo"))
      return nombreArchivo;
    else if (fieldName.equalsIgnoreCase("TIPO_INFORMACION") || fieldName.equals("tipoInformacion"))
      return tipoInformacion;
    else if (fieldName.equalsIgnoreCase("PROCEDIMIENTO"))
      return procedimiento;
    else if (fieldName.equalsIgnoreCase("STARTDATE"))
      return startdate;
    else if (fieldName.equalsIgnoreCase("ENDDATE"))
      return enddate;
    else if (fieldName.equalsIgnoreCase("AD_ORG_ID") || fieldName.equals("adOrgId"))
      return adOrgId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CrearAnexoData[] select(ConnectionProvider connectionProvider, String adProcesaId)    throws ServletException {
    return select(connectionProvider, adProcesaId, 0, 0);
  }

  public static CrearAnexoData[] select(ConnectionProvider connectionProvider, String adProcesaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT A.NOMBRE_XML, " +
      "               A.NOMBRE_ARCHIVO, " +
      "               AC.TIPO_INFORMACION, " +
      "               AC.PROCEDIMIENTO," +
      "               PS.STARTDATE, " +
      "               PS.ENDDATE," +
      "               PA.AD_ORG_ID " +
      "          FROM ATS_PROCESA_ANEXO PA," +
      "               C_PERIOD PS," +
      "               ATS_ANEXO A," +
      "               ATS_ANEXO_CONFIG AC" +
      "         WHERE PA.ATS_PROCESA_ANEXO_ID = ?" +
      "           AND PA.ATS_ANEXO_ID = A.ATS_ANEXO_ID" +
      "           AND AC.ATS_ANEXO_ID = A.ATS_ANEXO_ID" +
      "           AND A.ISACTIVE = 'Y'" +
      "           AND PA.C_PERIOD_ID = PS.C_PERIOD_ID" +
      "         ORDER BY AC.LINE ASC";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adProcesaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CrearAnexoData objectCrearAnexoData = new CrearAnexoData();
        objectCrearAnexoData.nombreXml = UtilSql.getValue(result, "NOMBRE_XML");
        objectCrearAnexoData.nombreArchivo = UtilSql.getValue(result, "NOMBRE_ARCHIVO");
        objectCrearAnexoData.tipoInformacion = UtilSql.getValue(result, "TIPO_INFORMACION");
        objectCrearAnexoData.procedimiento = UtilSql.getValue(result, "PROCEDIMIENTO");
        objectCrearAnexoData.startdate = UtilSql.getDateValue(result, "STARTDATE", "dd-MM-yyyy");
        objectCrearAnexoData.enddate = UtilSql.getDateValue(result, "ENDDATE", "dd-MM-yyyy");
        objectCrearAnexoData.adOrgId = UtilSql.getValue(result, "AD_ORG_ID");
        objectCrearAnexoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCrearAnexoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CrearAnexoData objectCrearAnexoData[] = new CrearAnexoData[vector.size()];
    vector.copyInto(objectCrearAnexoData);
    return(objectCrearAnexoData);
  }
}
