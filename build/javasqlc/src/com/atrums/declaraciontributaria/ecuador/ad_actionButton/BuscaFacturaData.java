//Sqlc generated V1.O00-1
package com.atrums.declaraciontributaria.ecuador.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class BuscaFacturaData implements FieldProvider {
static Logger log4j = Logger.getLogger(BuscaFacturaData.class);
  private String InitRecordNumber="0";
  public String cBpartnerId;
  public String docbasetype;
  public String cInvoiceId;
  public String total;
  public String tpidcliente;
  public String idcliente;
  public String tipocomprobante;
  public String numerocomprobantes;
  public String basenograiva;
  public String baseimponible;
  public String baseimpgrav;
  public String montoiva;
  public String valorretiva;
  public String valorretrenta;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("C_BPARTNER_ID") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("DOCBASETYPE"))
      return docbasetype;
    else if (fieldName.equalsIgnoreCase("C_INVOICE_ID") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("TOTAL"))
      return total;
    else if (fieldName.equalsIgnoreCase("TPIDCLIENTE"))
      return tpidcliente;
    else if (fieldName.equalsIgnoreCase("IDCLIENTE"))
      return idcliente;
    else if (fieldName.equalsIgnoreCase("TIPOCOMPROBANTE"))
      return tipocomprobante;
    else if (fieldName.equalsIgnoreCase("NUMEROCOMPROBANTES"))
      return numerocomprobantes;
    else if (fieldName.equalsIgnoreCase("BASENOGRAIVA"))
      return basenograiva;
    else if (fieldName.equalsIgnoreCase("BASEIMPONIBLE"))
      return baseimponible;
    else if (fieldName.equalsIgnoreCase("BASEIMPGRAV"))
      return baseimpgrav;
    else if (fieldName.equalsIgnoreCase("MONTOIVA"))
      return montoiva;
    else if (fieldName.equalsIgnoreCase("VALORRETIVA"))
      return valorretiva;
    else if (fieldName.equalsIgnoreCase("VALORRETRENTA"))
      return valorretrenta;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static BuscaFacturaData[] select(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssoyrx)    throws ServletException {
    return select(connectionProvider, fechaInicio, fechaFin, adClient, adIssoyrx, 0, 0);
  }

  public static BuscaFacturaData[] select(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssoyrx, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          SELECT  " +
      "          I.C_BPARTNER_ID," +
      "          D.DOCBASETYPE," +
      "          I.C_INVOICE_ID," +
      "		  '' as TOTAL," +
      "		  '' as tpIdCliente," +
      "		  '' as idCliente," +
      "		  '' as tipoComprobante," +
      "		  '' as numeroComprobantes," +
      "		  '' as baseNoGraIva," +
      "		  '' as baseImponible," +
      "		  '' as baseImpGrav," +
      "		  '' as montoIva," +
      "		  '' as valorRetIva," +
      "		  '' as valorRetRenta" +
      "          FROM C_INVOICE I, C_DOCTYPE D" +
      "         WHERE DOCSTATUS ='CO'" +
      "           AND DOCACTION = 'RE'" +
      "           AND PROCESSED = 'Y'" +
      "           AND DATEINVOICED >= ?" +
      "           AND DATEINVOICED <= ?" +
      "           AND I.AD_CLIENT_ID = ?" +
      "           AND I.ISSOTRX = ?" +
      "           AND I.C_DOCTYPETARGET_ID = D.C_DOCTYPE_ID" +
      "		   and I.c_doctypetarget_id NOT IN (select c_doctype_id from c_doctype where em_co_tp_comp_autorizador_sri = '0')";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adIssoyrx);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BuscaFacturaData objectBuscaFacturaData = new BuscaFacturaData();
        objectBuscaFacturaData.cBpartnerId = UtilSql.getValue(result, "C_BPARTNER_ID");
        objectBuscaFacturaData.docbasetype = UtilSql.getValue(result, "DOCBASETYPE");
        objectBuscaFacturaData.cInvoiceId = UtilSql.getValue(result, "C_INVOICE_ID");
        objectBuscaFacturaData.total = UtilSql.getValue(result, "TOTAL");
        objectBuscaFacturaData.tpidcliente = UtilSql.getValue(result, "TPIDCLIENTE");
        objectBuscaFacturaData.idcliente = UtilSql.getValue(result, "IDCLIENTE");
        objectBuscaFacturaData.tipocomprobante = UtilSql.getValue(result, "TIPOCOMPROBANTE");
        objectBuscaFacturaData.numerocomprobantes = UtilSql.getValue(result, "NUMEROCOMPROBANTES");
        objectBuscaFacturaData.basenograiva = UtilSql.getValue(result, "BASENOGRAIVA");
        objectBuscaFacturaData.baseimponible = UtilSql.getValue(result, "BASEIMPONIBLE");
        objectBuscaFacturaData.baseimpgrav = UtilSql.getValue(result, "BASEIMPGRAV");
        objectBuscaFacturaData.montoiva = UtilSql.getValue(result, "MONTOIVA");
        objectBuscaFacturaData.valorretiva = UtilSql.getValue(result, "VALORRETIVA");
        objectBuscaFacturaData.valorretrenta = UtilSql.getValue(result, "VALORRETRENTA");
        objectBuscaFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBuscaFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BuscaFacturaData objectBuscaFacturaData[] = new BuscaFacturaData[vector.size()];
    vector.copyInto(objectBuscaFacturaData);
    return(objectBuscaFacturaData);
  }

  public static BuscaFacturaData[] selectInvoice(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssoyrx)    throws ServletException {
    return selectInvoice(connectionProvider, fechaInicio, fechaFin, adClient, adIssoyrx, 0, 0);
  }

  public static BuscaFacturaData[] selectInvoice(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssoyrx, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          SELECT C_INVOICE_ID" +
      "          FROM C_INVOICE" +
      "         WHERE PROCESSED = 'Y'" +
      "           AND TO_DATE(DATEINVOICED+0) >= TO_DATE(?)" +
      "           AND TO_DATE(DATEINVOICED+0) <= TO_DATE(?)" +
      "           AND AD_CLIENT_ID = ?" +
      "           AND ISSOTRX = ?" +
      "		   and c_doctypetarget_id NOT IN (select c_doctype_id from c_doctype where em_co_tp_comp_autorizador_sri = '0')";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adIssoyrx);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BuscaFacturaData objectBuscaFacturaData = new BuscaFacturaData();
        objectBuscaFacturaData.cInvoiceId = UtilSql.getValue(result, "C_INVOICE_ID");
        objectBuscaFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBuscaFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BuscaFacturaData objectBuscaFacturaData[] = new BuscaFacturaData[vector.size()];
    vector.copyInto(objectBuscaFacturaData);
    return(objectBuscaFacturaData);
  }
}
