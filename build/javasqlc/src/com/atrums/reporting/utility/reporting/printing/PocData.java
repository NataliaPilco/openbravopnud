//Sqlc generated V1.O00-1
package com.atrums.reporting.utility.reporting.printing;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class PocData implements FieldProvider {
static Logger log4j = Logger.getLogger(PocData.class);
  private String InitRecordNumber="0";
  public String documentId;
  public String docstatus;
  public String ourreference;
  public String yourreference;
  public String salesrepUserId;
  public String salesrepEmail;
  public String salesrepName;
  public String bpartnerId;
  public String bpartnerName;
  public String contactUserId;
  public String contactEmail;
  public String contactName;
  public String adUserId;
  public String userEmail;
  public String userName;
  public String reportLocation;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("DOCUMENT_ID") || fieldName.equals("documentId"))
      return documentId;
    else if (fieldName.equalsIgnoreCase("DOCSTATUS"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("OURREFERENCE"))
      return ourreference;
    else if (fieldName.equalsIgnoreCase("YOURREFERENCE"))
      return yourreference;
    else if (fieldName.equalsIgnoreCase("SALESREP_USER_ID") || fieldName.equals("salesrepUserId"))
      return salesrepUserId;
    else if (fieldName.equalsIgnoreCase("SALESREP_EMAIL") || fieldName.equals("salesrepEmail"))
      return salesrepEmail;
    else if (fieldName.equalsIgnoreCase("SALESREP_NAME") || fieldName.equals("salesrepName"))
      return salesrepName;
    else if (fieldName.equalsIgnoreCase("BPARTNER_ID") || fieldName.equals("bpartnerId"))
      return bpartnerId;
    else if (fieldName.equalsIgnoreCase("BPARTNER_NAME") || fieldName.equals("bpartnerName"))
      return bpartnerName;
    else if (fieldName.equalsIgnoreCase("CONTACT_USER_ID") || fieldName.equals("contactUserId"))
      return contactUserId;
    else if (fieldName.equalsIgnoreCase("CONTACT_EMAIL") || fieldName.equals("contactEmail"))
      return contactEmail;
    else if (fieldName.equalsIgnoreCase("CONTACT_NAME") || fieldName.equals("contactName"))
      return contactName;
    else if (fieldName.equalsIgnoreCase("AD_USER_ID") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("USER_EMAIL") || fieldName.equals("userEmail"))
      return userEmail;
    else if (fieldName.equalsIgnoreCase("USER_NAME") || fieldName.equals("userName"))
      return userName;
    else if (fieldName.equalsIgnoreCase("REPORT_LOCATION") || fieldName.equals("reportLocation"))
      return reportLocation;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static PocData[] dummy(ConnectionProvider connectionProvider)    throws ServletException {
    return dummy(connectionProvider, 0, 0);
  }

  public static PocData[] dummy(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		select" +
      "			'' as document_id," +
      "			'' as docstatus," +
      "			'' as ourreference," +
      "			'' as yourreference," +
      "			'' as salesrep_user_id," +
      "			'' as salesrep_email," +
      "			'' as salesrep_name," +
      "			'' as bpartner_id," +
      "			'' as bpartner_name," +
      "			'' as contact_user_id," +
      "			'' as contact_email," +
      "			'' as contact_name," +
      "			'' as ad_user_id," +
      "			'' as user_email," +
      "			'' as user_name," +
      "			'' as report_location" +
      "		from" +
      "			dual";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "DOCUMENT_ID");
        objectPocData.docstatus = UtilSql.getValue(result, "DOCSTATUS");
        objectPocData.ourreference = UtilSql.getValue(result, "OURREFERENCE");
        objectPocData.yourreference = UtilSql.getValue(result, "YOURREFERENCE");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "SALESREP_USER_ID");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "SALESREP_EMAIL");
        objectPocData.salesrepName = UtilSql.getValue(result, "SALESREP_NAME");
        objectPocData.bpartnerId = UtilSql.getValue(result, "BPARTNER_ID");
        objectPocData.bpartnerName = UtilSql.getValue(result, "BPARTNER_NAME");
        objectPocData.contactUserId = UtilSql.getValue(result, "CONTACT_USER_ID");
        objectPocData.contactEmail = UtilSql.getValue(result, "CONTACT_EMAIL");
        objectPocData.contactName = UtilSql.getValue(result, "CONTACT_NAME");
        objectPocData.adUserId = UtilSql.getValue(result, "AD_USER_ID");
        objectPocData.userEmail = UtilSql.getValue(result, "USER_EMAIL");
        objectPocData.userName = UtilSql.getValue(result, "USER_NAME");
        objectPocData.reportLocation = UtilSql.getValue(result, "REPORT_LOCATION");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsForRetenciones(ConnectionProvider connectionProvider, String cRetencionCompraId)    throws ServletException {
    return getContactDetailsForRetenciones(connectionProvider, cRetencionCompraId, 0, 0);
  }

  public static PocData[] getContactDetailsForRetenciones(ConnectionProvider connectionProvider, String cRetencionCompraId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select" +
      "            co_retencion_compra.co_retencion_compra_id as document_id," +
      "            co_retencion_compra.docstatus as docstatus," +
      "            co_retencion_compra.documentno as ourreference," +
      "            null as yourreference," +
      "            c_invoice.salesrep_id as salesrep_user_id," +
      "            salesrep.email as salesrep_email," +
      "            salesrep.name as salesrep_name," +
      "            c_invoice.c_bpartner_id as bpartner_id," +
      "            c_bpartner.name as bpartner_name," +
      "            c_invoice.ad_user_id as contact_user_id," +
      "            customercontact.email as contact_email," +
      "            customercontact.name as contact_name" +
      "        from co_retencion_compra left join c_invoice on co_retencion_compra.c_invoice_id = c_invoice.c_invoice_id" +
      "                                 left join ad_user customercontact on c_invoice.ad_user_id = customercontact.ad_user_id" +
      "                                 left join ad_user salesrep on c_invoice.salesrep_id = salesrep.ad_user_id" +
      "                                 left join c_bpartner on c_invoice.c_bpartner_id = c_bpartner.c_bpartner_id" +
      "        where" +
      "			1=1";
    strSql = strSql + ((cRetencionCompraId==null || cRetencionCompraId.equals(""))?"":"              and co_retencion_compra.co_retencion_compra_id in          " + cRetencionCompraId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (cRetencionCompraId != null && !(cRetencionCompraId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "DOCUMENT_ID");
        objectPocData.docstatus = UtilSql.getValue(result, "DOCSTATUS");
        objectPocData.ourreference = UtilSql.getValue(result, "OURREFERENCE");
        objectPocData.yourreference = UtilSql.getValue(result, "YOURREFERENCE");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "SALESREP_USER_ID");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "SALESREP_EMAIL");
        objectPocData.salesrepName = UtilSql.getValue(result, "SALESREP_NAME");
        objectPocData.bpartnerId = UtilSql.getValue(result, "BPARTNER_ID");
        objectPocData.bpartnerName = UtilSql.getValue(result, "BPARTNER_NAME");
        objectPocData.contactUserId = UtilSql.getValue(result, "CONTACT_USER_ID");
        objectPocData.contactEmail = UtilSql.getValue(result, "CONTACT_EMAIL");
        objectPocData.contactName = UtilSql.getValue(result, "CONTACT_NAME");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsForUser(ConnectionProvider connectionProvider, String adUserId)    throws ServletException {
    return getContactDetailsForUser(connectionProvider, adUserId, 0, 0);
  }

  public static PocData[] getContactDetailsForUser(ConnectionProvider connectionProvider, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select" +
      "            ad_user.ad_user_id," +
      "            ad_user.email as user_email," +
      "            ad_user.name as user_name" +
      "        from" +
      "            ad_user" +
      "        where" +
      "            ad_user.ad_user_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.adUserId = UtilSql.getValue(result, "AD_USER_ID");
        objectPocData.userEmail = UtilSql.getValue(result, "USER_EMAIL");
        objectPocData.userName = UtilSql.getValue(result, "USER_NAME");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }

  public static PocData[] getContactDetailsMormovement(ConnectionProvider connectionProvider, String mMovementId)    throws ServletException {
    return getContactDetailsMormovement(connectionProvider, mMovementId, 0, 0);
  }

  public static PocData[] getContactDetailsMormovement(ConnectionProvider connectionProvider, String mMovementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "         select m_movement.m_movement_id as document_id," +
      "                'CL' as docstatus," +
      "                m_movement.documentno as ourreference," +
      "                null as yourreference," +
      "                '' as salesrep_user_id," +
      "                '' as salesrep_email," +
      "                '' as salesrep_name," +
      "                '' as bpartner_id," +
      "                '' as bpartner_name," +
      "                m_movement.createdby as contact_user_id," +
      "                '' as contact_email," +
      "                '' as contact_name" +
      "           from m_movement " +
      "          where 1=1";
    strSql = strSql + ((mMovementId==null || mMovementId.equals(""))?"":"              and m_movement.m_movement_id in          " + mMovementId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (mMovementId != null && !(mMovementId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PocData objectPocData = new PocData();
        objectPocData.documentId = UtilSql.getValue(result, "DOCUMENT_ID");
        objectPocData.docstatus = UtilSql.getValue(result, "DOCSTATUS");
        objectPocData.ourreference = UtilSql.getValue(result, "OURREFERENCE");
        objectPocData.yourreference = UtilSql.getValue(result, "YOURREFERENCE");
        objectPocData.salesrepUserId = UtilSql.getValue(result, "SALESREP_USER_ID");
        objectPocData.salesrepEmail = UtilSql.getValue(result, "SALESREP_EMAIL");
        objectPocData.salesrepName = UtilSql.getValue(result, "SALESREP_NAME");
        objectPocData.bpartnerId = UtilSql.getValue(result, "BPARTNER_ID");
        objectPocData.bpartnerName = UtilSql.getValue(result, "BPARTNER_NAME");
        objectPocData.contactUserId = UtilSql.getValue(result, "CONTACT_USER_ID");
        objectPocData.contactEmail = UtilSql.getValue(result, "CONTACT_EMAIL");
        objectPocData.contactName = UtilSql.getValue(result, "CONTACT_NAME");
        objectPocData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPocData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PocData objectPocData[] = new PocData[vector.size()];
    vector.copyInto(objectPocData);
    return(objectPocData);
  }
}
