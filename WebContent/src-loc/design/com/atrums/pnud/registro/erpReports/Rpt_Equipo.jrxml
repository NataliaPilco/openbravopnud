<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_Equipo" pageWidth="595" pageHeight="842" whenNoDataType="BlankPage" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DOCUMENT_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_DESIGN" class="java.lang.String">
		<defaultValueExpression><![CDATA["C:\\Trabajo-Consultoria\\PNUD\\OpenbravoERP\\modules\\com.atrums.pnud.registro\\src"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT *
FROM (SELECT
  CASE WHEN PRE.ESTATUS IS NOT NULL THEN PRE.ESTATUS ELSE 'Libre de PCB' END AS ESTATUS,
  CASE WHEN PUP.FECHA_UBICACION IS NOT NULL THEN ' ' || TO_CHAR(PUP.FECHA_UBICACION,'dd') || ' de ' ||
  TRIM(TO_CHAR(PUP.FECHA_UBICACION, 'month','nls_date_language=spanish')) || ' de ' ||
  TO_CHAR(PUP.FECHA_UBICACION,'yyyy') ELSE NULL END AS FECHA,
  CASE WHEN CBR.NAME IS NOT NULL THEN ' ' || INITCAP(CBR.NAME) ELSE NULL END AS PROPIETARIO,
  CASE WHEN PMA.MARCA IS NOT NULL THEN ' ' || INITCAP(PMA.MARCA) ELSE NULL END AS FABRICANTE,
  CASE WHEN CCY.NAME IS NOT NULL THEN ' ' || INITCAP(CCY.NAME) ELSE NULL END AS PAIS_FABRICANTE,
  CASE WHEN PCE.TIPO_EQUIPO IS NOT NULL THEN ' ' || INITCAP(PCE.TIPO_EQUIPO) ELSE NULL END AS TIPO_EQUIPO,
  CASE WHEN PRE.NRO_FASES = 'P1' THEN ' Fases: 1' WHEN PRE.NRO_FASES = 'P2' THEN ' Fases: 2' ELSE ' Fases: 3' END AS FASES,
  CASE WHEN PRE.TIPO_TRANSFORMADOR = 'PD' THEN ' Distribución' ELSE ' Potencia' END AS TIPO_TRASFORMADOR,
  CASE WHEN PRE.KVA IS NOT NULL THEN ' ' || PRE.KVA ELSE NULL END AS POTENCIA,
  CASE WHEN CYR.YEAR IS NOT NULL THEN ' ' || CYR.YEAR ELSE NULL END AS ANIO_FABRICACION,
  CASE WHEN PRE.NRO_SERIE IS NOT NULL THEN ' ' || PRE.NRO_SERIE ELSE NULL END AS NRO_SERIE,
  CASE WHEN PRE.NRO_EMPRESA IS NOT NULL THEN ' ' || PRE.NRO_EMPRESA ELSE NULL END AS NRO_EMPRESA,
  CASE WHEN PRE.EQUIPO_ACEITE IS NOT NULL THEN ' ' || PRE.EQUIPO_ACEITE ELSE NULL END AS EQUIPO_PESO,
  CASE WHEN PRE.ACEITE IS NOT NULL THEN ' ' || PRE.ACEITE ELSE NULL END AS ACEITE,
  CASE WHEN PRE.EQUIPO_ACEITE IS NOT NULL THEN ' ' || (PRE.EQUIPO_ACEITE + PRE.ACEITE) ELSE NULL END AS EQUIPO_CON_ACEITE,
  CASE WHEN PRE.LITROS IS NOT NULL THEN ' ' || PRE.LITROS ELSE NULL END AS VOLUMEN_ACEITE,
  CASE WHEN PUP.NRO_PORTE IS NOT NULL THEN ' ' || PUP.NRO_PORTE ELSE NULL END AS POSTE,
  CASE WHEN MWH.C_LOCATION_ID IS NOT NULL THEN ' ' || CRW.NAME || '/' || CCW.NAME || '/' || PPW.NAME || '/' || CLW.ADDRESS1 ELSE NULL END AS BODEGA,
  CASE WHEN PUP.LATITUD IS NOT NULL THEN ' X: ' || PUP.LATITUD || ' / Y: ' || PUP.LONGITUD ELSE NULL END AS UTM,
  CASE WHEN PPP.PNUDR_PRUEBAS_ID IS NOT NULL THEN ' ' || CASE WHEN PPP.RESULTADO = 'PME' THEN '< 50 ppm' ELSE '> 50 ppm' END ELSE NULL END AS ANALISIS_CUALITATIVO,
  CASE WHEN PPP.CONCENTRACION IS NOT NULL THEN ' ' || PPP.CONCENTRACION || ' ppm' ELSE NULL END AS CONCENTRACION,
  CASE WHEN PPP.FECHA_MUESTRA IS NOT NULL THEN ' ' || PPP.FECHA_MUESTRA ELSE NULL END AS MUESTRA
FROM PNUDR_REG_EQUIPO PRE
     LEFT JOIN PNUDR_UBICPROPPESO PUP ON (PRE.PNUDR_REG_EQUIPO_ID = PUP.PNUDR_REG_EQUIPO_ID)
     LEFT JOIN PNUDR_PRUEBAS PPP ON (PRE.PNUDR_REG_EQUIPO_ID = PPP.PNUDR_REG_EQUIPO_ID)
     LEFT JOIN C_BPARTNER CBR ON (PUP.C_BPARTNER_ID = CBR.C_BPARTNER_ID)
     LEFT JOIN PNUDR_MARCA PMA ON (PRE.PNUDR_MARCA_ID = PMA.PNUDR_MARCA_ID)
     LEFT JOIN C_COUNTRY CCY ON (PRE.C_COUNTRY_ID = CCY.C_COUNTRY_ID)
     LEFT JOIN PNUDR_CONFG_EQUIPO PCE ON (PRE.PNUDR_CONFG_EQUIPO_ID = PCE.PNUDR_CONFG_EQUIPO_ID)
     LEFT JOIN C_YEAR CYR ON (PRE.C_YEAR_ID = CYR.C_YEAR_ID)
     LEFT JOIN M_WAREHOUSE MWH ON (PUP.M_WAREHOUSE_ID = MWH.M_WAREHOUSE_ID)
     LEFT JOIN C_LOCATION CLW ON (MWH.C_LOCATION_ID = CLW.C_LOCATION_ID)
     LEFT JOIN C_REGION CRW ON (CLW.C_REGION_ID = CRW.C_REGION_ID)
     LEFT JOIN C_CITY CCW ON (CLW.C_CITY_ID = CCW.C_CITY_ID)
     LEFT JOIN PNUDR_PARROQUIAS PPW ON (CLW.EM_PNUDR_PARROQUIAS_ID = PPW.PNUDR_PARROQUIAS_ID)
WHERE PRE.PNUDR_REG_EQUIPO_ID = $P{DOCUMENT_ID}
ORDER BY PUP.FECHA_UBICACION DESC, PPP.FECHA_MUESTRA DESC)
WHERE ROWNUM = 1]]>
	</queryString>
	<field name="ESTATUS" class="java.lang.String"/>
	<field name="FECHA" class="java.lang.String"/>
	<field name="PROPIETARIO" class="java.lang.String"/>
	<field name="FABRICANTE" class="java.lang.String"/>
	<field name="PAIS_FABRICANTE" class="java.lang.String"/>
	<field name="TIPO_EQUIPO" class="java.lang.String"/>
	<field name="FASES" class="java.lang.String"/>
	<field name="TIPO_TRASFORMADOR" class="java.lang.String"/>
	<field name="POTENCIA" class="java.lang.String"/>
	<field name="ANIO_FABRICACION" class="java.lang.String"/>
	<field name="NRO_SERIE" class="java.lang.String"/>
	<field name="NRO_EMPRESA" class="java.lang.String"/>
	<field name="EQUIPO_PESO" class="java.lang.String"/>
	<field name="ACEITE" class="java.lang.String"/>
	<field name="EQUIPO_CON_ACEITE" class="java.lang.String"/>
	<field name="VOLUMEN_ACEITE" class="java.lang.String"/>
	<field name="POSTE" class="java.lang.String"/>
	<field name="BODEGA" class="java.lang.String"/>
	<field name="UTM" class="java.lang.String"/>
	<field name="ANALISIS_CUALITATIVO" class="java.lang.String"/>
	<field name="CONCENTRACION" class="java.lang.String"/>
	<field name="MUESTRA" class="java.lang.String"/>
	<pageHeader>
		<band height="53">
			<image>
				<reportElement x="0" y="1" width="145" height="52"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<imageExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN}.replaceFirst( "src-loc/design", "web" ).replaceFirst( "\\src","\\web" ) + "/com.atrums.pnud.registro/images/descarga_2.png"]]></imageExpression>
			</image>
			<image>
				<reportElement x="384" y="1" width="171" height="52"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<imageExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN}.replaceFirst( "src-loc/design", "web" ).replaceFirst( "\\src","\\web" ) + "/com.atrums.pnud.registro/images/descarga_4.jpg"]]></imageExpression>
			</image>
			<staticText>
				<reportElement mode="Opaque" x="145" y="1" width="239" height="52" forecolor="#000000" backcolor="#FF0000"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="13" isBold="true"/>
				</textElement>
				<text><![CDATA[CONTIENE PCB]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="145" y="1" width="239" height="52" forecolor="#000000" backcolor="#33CC00">
					<printWhenExpression><![CDATA[$F{ESTATUS}.matches("Libre de PCB")]]></printWhenExpression>
				</reportElement>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font size="13" isBold="true"/>
				</textElement>
				<text><![CDATA[LIBRE DE PCB]]></text>
			</staticText>
		</band>
	</pageHeader>
	<detail>
		<band height="539">
			<staticText>
				<reportElement x="0" y="0" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Fecha ]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="0" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{FECHA}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="20" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Propietario]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="40" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Nombre del fabricante]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="60" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ País de origen]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="80" width="347" height="59"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Tipo de equipo]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="139" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Potencia (KVA)]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="20" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{PROPIETARIO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="40" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{FABRICANTE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="60" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{PAIS_FABRICANTE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="80" width="208" height="32"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{TIPO_EQUIPO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="112" width="208" height="27"/>
				<box>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{FASES}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="159" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Año de fabricación]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="179" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ No. de serie]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="199" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ No. de empresa]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="219" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Nombre del líquido o aceite aislante/refrigerante]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="239" width="157" height="60"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Peso (kg)]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="239" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Equipo (peso seco)]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="259" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Aceite]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="279" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Equipo con aceite]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="299" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Volumen de aceite (L)]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="319" width="157" height="60"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Ubicación]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="319" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Poste]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="339" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Bodega]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="359" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Coordenadas UTM]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="379" width="157" height="40"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Determinación de PCB]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="379" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Análisis cualitativo (kit colorimétrico)]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="399" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Análisis cualitativo (cromatografía)]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="419" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Fuente de informacióon anterior (ej.: placa o rótulo en el equipo)]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="439" width="157" height="40"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Situación operativa del equipo]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="439" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ En uso]]></text>
			</staticText>
			<staticText>
				<reportElement x="157" y="459" width="190" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Dado de baja]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="479" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Situación operativa del equipo]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="499" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Registro fotográfico]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="519" width="347" height="20"/>
				<box>
					<pen lineWidth="0.5" lineStyle="Solid"/>
					<topPen lineWidth="0.5" lineStyle="Solid"/>
					<leftPen lineWidth="0.5" lineStyle="Solid"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid"/>
					<rightPen lineWidth="0.5" lineStyle="Solid"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[ Observaciones]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="159" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ANIO_FABRICACION}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="179" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{NRO_SERIE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="199" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{NRO_EMPRESA}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="347" y="219" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="239" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{EQUIPO_PESO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="259" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ACEITE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="299" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{VOLUMEN_ACEITE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="319" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{POSTE}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="339" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{BODEGA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="359" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{UTM}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="379" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ANALISIS_CUALITATIVO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="399" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{CONCENTRACION}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="347" y="419" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<staticText>
				<reportElement x="347" y="459" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<staticText>
				<reportElement x="347" y="439" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<staticText>
				<reportElement x="347" y="479" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<staticText>
				<reportElement x="347" y="499" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<staticText>
				<reportElement x="347" y="519" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[ N/A]]></text>
			</staticText>
			<textField>
				<reportElement x="347" y="139" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{POTENCIA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="347" y="279" width="208" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{EQUIPO_CON_ACEITE}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
