package com.atrums.pnud.registro.ad_process;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.quartz.JobExecutionException;

public class PNUDR_Generar_Roles implements Process {

  final OBError msg = new OBError();
  private ConnectionProvider connection;
  private ProcessLogger logger;

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    // TODO Auto-generated method stub
    logger = bundle.getLogger();
    connection = bundle.getConnection();

    try {

      String strAdClient = bundle.getContext().getClient();
      String strAdOrg = bundle.getContext().getOrganization();

      if (strAdClient.equals("")) {
        strAdClient = bundle.getParams().get("adClientId").toString();
      }

      if (strAdOrg.equals("")) {
        strAdOrg = bundle.getParams().get("adOrgId").toString();
      }

      if (!strAdOrg.equals("") && !strAdClient.equals("")) {
        PNUDR_Util_Procedure_Rol.ejecutaFuncion(connection, "pnudr_generar_roles", strAdOrg,
            strAdClient);
      }
      // PNUDR_Util_Procedure.ejecutaFuncion(connection, "pnudr_procesar_equipo", strId);

      msg.setType("Success");
      msg.setTitle("Done");
      msg.setMessage("Proceso Finalizado Correctamente");
    } catch (Exception e) {
      msg.setType("Error");
      msg.setMessage("No se proceso ninguna línea - " + e.getMessage());
      msg.setTitle("Procesar Error");
      throw new JobExecutionException(e.getMessage(), e);
    } finally {
      logger.log("Proceso Finalizado ");
    }

    bundle.setResult(msg);
  }

}
