package com.atrums.pnud.registro.ad_process;

import java.sql.CallableStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;

public class PNUDR_Util_Procedure_Rol implements FieldProvider {
  static Logger log4j = Logger.getLogger(PNUDR_Util_Procedure.class);

  @Override
  public String getField(String fieldName) {
    // TODO Auto-generated method stub
    return null;
  }

  public static void ejecutaFuncion(ConnectionProvider connectionProvider, String srtrProccedure,
      String strAdOrg, String strAdClient) throws ServletException {
    String strSql = "CALL " + srtrProccedure.toUpperCase() + "(?,?)";

    CallableStatement st = null;

    try {
      st = connectionProvider.getCallableStatement(strSql);
      UtilSql.setValue(st, 1, 12, null, strAdOrg);
      UtilSql.setValue(st, 2, 12, null, strAdClient);
      st.execute();
    } catch (SQLException e) {
      log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@"
          + e.getMessage());
    } catch (Exception ex) {
      log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch (Exception ignore) {
        ignore.printStackTrace();
      }
    }
  }

}