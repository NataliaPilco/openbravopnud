package com.atrums.pnud.registro.erpReports;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;

public class Rpt_Equipo extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strpnudrregequipoId = vars.getSessionValue("Rpt_Equipo.inppnudrRegEquipoId_R");
      if (strpnudrregequipoId.equals(""))
        strpnudrregequipoId = vars.getSessionValue("inppnudrRegEquipoId");
      if (strpnudrregequipoId.equals(""))
        strpnudrregequipoId = vars.getStringParameter("inppnudrRegEquipoId");
      if (log4j.isDebugEnabled())
        log4j.debug("strcOrderId" + strpnudrregequipoId);
      printPagePartePDF(response, vars, strpnudrregequipoId);
    } else
      pageError(response);
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strpnudrregequipoId) throws ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: pdf");
    // JasperPrint jasperPrint;

    String strReportName = "@basedesign@/com/atrums/pnud/registro/erpReports/Rpt_Equipo.jrxml";

    response.setHeader("Content-disposition", "inline; filename=Rpt_Equipo.pdf");

    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("DOCUMENT_ID", strpnudrregequipoId);

    renderJR(vars, response, strReportName, "pdf", parameters, null, null);
  }

  public String getServletInfo() {
    return "Servlet that presents the RptCOrders seeker";
  } // End of getServletInfo() method
}
