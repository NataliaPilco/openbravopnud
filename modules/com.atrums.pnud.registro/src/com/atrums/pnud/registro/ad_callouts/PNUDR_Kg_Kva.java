package com.atrums.pnud.registro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PNUDR_Kg_Kva extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strEquipoAceite = vars.getStringParameter("inpequipoAceite");
      String strAceite = vars.getStringParameter("inpaceite");
      String strIdEquipo = vars.getStringParameter("inppnudrRegEquipoId");
      try {
        if (strEquipoAceite != null && strIdEquipo != null)
          printPage(response, vars, strEquipoAceite, strIdEquipo, strAceite);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strEquipoAceite, String strIdEquipo, String strAceite) throws IOException,
      ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/pnud/registro/ad_callouts/CallOut").createXmlDocument();

    PNUDRKgKvaData[] data = PNUDRKgKvaData.select(this, strIdEquipo);

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='PNUDR_Kg_Kva';\n\n");

    result.append("var respuesta = new Array(");

    float fltESAceite = Float.valueOf(strEquipoAceite) - Float.valueOf(strAceite);

    result.append("new Array(\"inpequipoSinAceite\", \"" + fltESAceite + "\"),");

    if (Float.valueOf(data[0].getField("kva")) > 0) {
      float fltkva = Float.valueOf(strEquipoAceite) / Float.valueOf(data[0].getField("kva"));
      result.append("new Array(\"inpkva\", \"" + fltkva + "\")");
    } else {
      result.append("new Array(\"inpkva\", \"" + "\")");
    }

    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

}
