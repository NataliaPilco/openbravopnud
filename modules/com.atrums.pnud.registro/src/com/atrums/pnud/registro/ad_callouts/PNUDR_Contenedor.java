package com.atrums.pnud.registro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PNUDR_Contenedor extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strPnudrConfgEquipoId = vars.getStringParameter("inppnudrConfgEquipoId");
      try {
        if (strPnudrConfgEquipoId != null)
          printPage(response, vars, strPnudrConfgEquipoId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strPnudrConfgEquipoId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/pnud/registro/ad_callouts/CallOut").createXmlDocument();

    PNUDRContenedorData[] data = PNUDRContenedorData.select(this, strPnudrConfgEquipoId);

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='PNUDR_Contenedor';\n\n");

    result.append("var respuesta = new Array(");

    result.append("new Array(\"inpcapacidad\", \"" + data[0].getField("capacidad_lt") + "\"),");

    result.append("new Array(\"inpcapacidadGal\", \"" + data[0].getField("capacidad_gl") + "\")");
    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
