package com.atrums.pnud.registro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PNUDR_Conv_Equi_kva extends HttpSecureAppServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strEquipoAceite = vars.getStringParameter("inpequipoAceite");
      String strAceite = vars.getStringParameter("inpaceite");
      String strkva = vars.getStringParameter("inpkva");
      try {
        if (strEquipoAceite != null && strkva != null)

          if (strEquipoAceite.equals("") || strEquipoAceite.equals(null)) {
            strEquipoAceite = "0";
          } else if (strkva.equals("") || strkva.equals(null)) {
            strkva = "0";
          } else if (strAceite.equals("") || strAceite.equals(null)) {
            strEquipoAceite = "0";
          }

        printPage(response, vars, strEquipoAceite, strkva, strAceite);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strEquipoAceite, String strkva, String strAceite) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/pnud/registro/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='PNUDR_Kg_Kva';\n\n");

    result.append("var respuesta = new Array(");

    float fltESAceite = Float.valueOf(strEquipoAceite) - Float.valueOf(strAceite);
    result.append("new Array(\"inpequipoSinAceite\", \"" + fltESAceite + "\"),");

    float fltkvakg = Float.valueOf(strEquipoAceite) / Float.valueOf(strkva);

    if (Float.isInfinite(fltkvakg) || Float.isNaN(fltkvakg)) {
      fltkvakg = 0;
    }

    result.append("new Array(\"inpkvakg\", \"" + fltkvakg + "\")");

    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
