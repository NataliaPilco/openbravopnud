package com.atrums.pnud.registro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PNUDR_Tra_Equipo extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strpnudrRegEquipoId = vars.getStringParameter("inppnudrRegEquipoId");
      try {
        if (strpnudrRegEquipoId != null)
          printPage(response, vars, strpnudrRegEquipoId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strpnudrRegEquipoId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/pnud/registro/ad_callouts/CallOut").createXmlDocument();

    PNUDRTraEquipoData[] data = PNUDRTraEquipoData.select(this, strpnudrRegEquipoId);

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='PNUDR_Tra_Equipo';\n\n");

    result.append("var respuesta = new Array(");

    result.append("new Array(\"inpnroSerie\", \"" + data[0].getField("nro_serie") + "\"),");
    result.append("new Array(\"inpnroEmpresa\", \"" + data[0].getField("nro_empresa") + "\"),");
    result.append("new Array(\"inppnudrConfgEquipoId\", \"" + data[0].getField("tipo") + "\")");
    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
