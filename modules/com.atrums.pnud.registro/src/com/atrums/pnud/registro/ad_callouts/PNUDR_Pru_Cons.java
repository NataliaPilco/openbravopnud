package com.atrums.pnud.registro.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class PNUDR_Pru_Cons extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strConce = vars.getStringParameter("inpconcentracion");
      String strTipo = vars.getStringParameter("inptipoPrueba");
      try {
        if (strConce != null && strTipo != null)
          printPage(response, vars, strTipo, strConce);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strTipo,
      String strConce) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/atrums/pnud/registro/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='PNUDR_Pru_Cons';\n\n");

    result.append("var respuesta = new Array(");

    if (strTipo.equals("P2") && (Float.valueOf(strConce) < 50)) {
      result.append("new Array(\"inptratamiento\", \"" + "PCO" + "\")");
    } else if (strTipo.equals("P2") && (Float.valueOf(strConce) >= 50)) {
      result.append("new Array(\"inptratamiento\", \"" + "PCA" + "\")");
    }

	if (strTipo.equals("P3") && (Float.valueOf(strConce) < 50)) {
      result.append("new Array(\"inpresultado\", \"" + "PME" + "\")");
    } else if (strTipo.equals("P3") && (Float.valueOf(strConce) >= 50)) {
      result.append("new Array(\"inpresultado\", \"" + "PMA" + "\")");
    } 
	
    result.append(");");

    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }
}
